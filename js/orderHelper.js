//order selection starts here

 

//variables for pagination
				var list = new Array();
				var pageList = new Array();
				var currentPage = 1;
				var numPerPage = 10;
				var numberOfPages = 1;  // calculates the total number of pages
		
				var _sortByElement = 'createdDate-';
		 
			 	var orderSortIncr = true;
				var searchQuery = null;
				var orderSortCustIncr = true;

		        var _orderStatus = ["PPDI", "PDI", "APDI"];

//		var _orderStatus = ["PPDI", "PDI", "APDI"]; 
// 2 as default value for pdi selection for all excepr pdi, pre pdi and all pdi
//0 for pre pdi, 1 for pdi, 2 for aps, 3 for all
                var pdiSelection = 2;//2
	
//class orderdata 
		function OrderData2 (id, orderdisplayName, houseType, datecreated, customerName, addressofInspectedHouse, unitEnrollmentNum, dateOfPossession, vendorRef, lotNo, PlanNo, Municipality, condoProjectName, condoLevel, condoUnit, vendorName, representativeName, purchaserName2, DesignateName, FullOrder ) { 
				this.id = id;
				this.orderdisplayName = orderdisplayName;
				this.houseType = houseType;
				this.datecreated = datecreated;
				this.customerName = customerName;
				this.addressofInspectedHouse = addressofInspectedHouse; 
				this.unitEnrollmentNum = unitEnrollmentNum; 
				this.dateOfPossession = dateOfPossession; 
				
				this.vendorRef = vendorRef; 
				this.lotNo = lotNo; 
				this.PlanNo = PlanNo; 
				this.Municipality = Municipality; 
				this.condoProjectName = condoProjectName; 
				this.condoLevel = condoLevel; 
				this.condoUnit = condoUnit; 
				
				this.vendorName = vendorName; 
				this.representativeName = representativeName;  
				this.purchaserName2 = purchaserName2; 
				this.DesignateName = DesignateName; 
				this.FullOrder = FullOrder;  
		}
		

	//Order UI sorting functions	
				function loadOrderUIElements(){  
					refreshLocalVariables();
                    
                    //make sure the order nav buttons are visible and pdi are hidden 
                        $(".main_Navbar").removeClass("hidden");
                        $(".pdi_Navbar").addClass("hidden");
                    
//                    console.log("loading order list");
						try{
                    //reset search
                            $("#searchOrder").val('');
                        }catch(err){}
                    
					//clear order list
				$("#ordersListed table tbody").empty();
                     
					//no of items per page drop down trigger 
						$('#listofOrders').on('change', function() { 
								refreshLocalVariables();
								loadOrdersBYCriteria(searchQuery, _sortByElement, "", pdiSelection);
								incrementtest = 1;
						})
						  
						$("#orderID").unbind().click(function(){
								if(orderSortIncr){
									_sortByElement = 'createdDate+';
								}else{
									_sortByElement = 'createdDate-';
								}
								orderSortIncr = !orderSortIncr;
								loadOrdersBYCriteria(searchQuery, _sortByElement, "", pdiSelection);
						}); 
						
						$("#_ordercustomerName").unbind().click(function(){
								if(orderSortIncr){
									_sortByElement = 'customerName+';
								}else{
									_sortByElement = 'customerName-';
								}
								orderSortIncr = !orderSortIncr;
								loadOrdersBYCriteria(searchQuery, _sortByElement, "", pdiSelection);
						});
						
						
						try{
						$(".searchbtn").unbind().click(function(){
									
								refreshLocalVariables();
							  // do something
								searchQuery =  $("#searchOrder").val(); 
						//	console.log('pressed 2' + searchQuery);
								loadOrdersBYCriteria(searchQuery, _sortByElement, "", pdiSelection); 
								incrementtest = 1;
								
						});
						$("#searchOrder").unbind().keyup(function(ev) {
							   // 13 is ENTER
							if (ev.which === 13 ) {
								
								refreshLocalVariables();
							  // do something
								searchQuery =  $(this).val(); 
							//	console.log('pressed 2' + searchQuery);
								loadOrdersBYCriteria(searchQuery, _sortByElement, ""); 
								incrementtest = 1;
							   }
							});  
                        }catch(err){}
                            
                    
					loadOrdersBYCriteria("", 'createdDate-', "", pdiSelection);  
                    
                    //order page item click
                    $("#ordersListed tbody").on("click", "tr", function(e){  
                        //	console.log($(this)); 
                         var order_id = $(this).attr('id');
//                        console.log(order_id); 
                         var order_tr_data = $( "#"+order_id ).data( "wholeOrderStorage" );
                    	   //console.log($( order_tr_data));

                         //global var set
                         selectedOrderId = order_tr_data.id;
                        
                        //passing whole order json object and storing in cart_house_Id
                         addItemToCart(order_tr_data);
                          
                          
                        
                         switchUI(2);  
                    });
                    
			} 


		//refresh everything before and after login	
		 function refreshLocalVariables(){  
//                    console.log("clearing cache variables");
			  //reset variables  
				 selectedOrderId = null;
				 prePDIselected = null;
              
				 
				list = new Array();
				pageList = new Array();
				currentPage = 1;
				numPerPage = 10;
				numberOfPages = 1;  // calculates the total number of pages
		
				_sortByElement = 'createdDate-';
		 
			 	orderSortIncr = true;
				searchQuery = "";
				orderSortCustIncr = true; 
		 }


//function to change pdi selection through nav menu
        function pdiSelectBy(value){ 
//            console.log("value received "+ this); 
            pdiSelection = value; 
            loadOrdersBYCriteria("", 'createdDate-', "", value);   
        }


  

	//Order selection page starts	
				function loadOrdersBYCriteria(_query,_sortby, _indexBy, _pdiSelection){
					  
                    
					var _sortby = _sortby == "" ? 'createdDate-' : _sortby ;
					var _indexBy = _indexBy == "" ? 'ORDERTYPE' : _indexBy ; 
                    
                    var __pdiSelection = 2;
                    if(_pdiSelection != null){
                        __pdiSelection = _pdiSelection;
                    }
                    
//					var _pdiSelection = _pdiSelection ? 2 : _pdiSelection ;  //2  to show all but except prepdi or pdi and all pdi
//					 $('#result').addClass('alert-warning')
//                                .removeClass('alert-success hidden').text("loading...");
                    
//                    console.log("_pdiSelection "+ __pdiSelection); 
					if(_query == null || _query ==""){
                        	kV.getAllOrdersListNoQuery(_sortby, _indexBy, __pdiSelection).done(function (result) { 
                //   kV.multiPageSearchRequest(_query,_sortby, _indexBy, _from, _to).done(function (result) {  
						var orderIDs = [];
						try{
							 var myRecords = JSON.parse(result);
//							 console.log(myRecords);  
                            
							 for(var key in myRecords){  
								var subItem = myRecords[key].item;  
								for(var key2 in subItem){   
									var matchSuccessSrc = false;
									var matchSuccessTpe = false; 
									var orderID = subItem[key2].id;  
									if (orderID.match(/^\d+$/) && orderID != null){  
										var fieldItems = subItem[key2].field;
											for(var key3 in fieldItems){ 
												var fieldItem = fieldItems[key3];
										//		console.log("fieldItem "+fieldItem);
										//	console.log(fieldItem);  
                                                
                                            if(fieldItem.name== "orderSource"){ 
                                                
//												console.log(fieldItem.value);
//                    console.log("_pdiSelection "+ __pdiSelection);
                                                
//                                                var _orderStatus = ["PPDI", "PDI", "APDI"]; 
                                                switch(__pdiSelection){
                                                    case 0: // show pre pdi   
//                                                            console.log(fieldItem.value);
//                                                                for(var key4 in fieldItem.value){
//                                                                    var currentOrderType = fieldItem.value[key4].toLowerCase(); 
////                                                                    console.log(currentOrderType +" " +(_orderStatus[0]).toLowerCase()); 
//                                                                    if(currentOrderType == (_orderStatus[0]).toLowerCase() )
//                                                                    {
//                                                                        matchSuccessSrc = true;
////                                                                        console.log(fieldItem + " compare " +(_orderStatus[0]).toLowerCase()); 
//                                                                       } 
//                                                                }
                                                         matchSuccessSrc = true;
                                                        break;
                                                        
                                                    case  1: ///show only pdi
//                                                            console.log(fieldItem.value);
//                                                                for(var key4 in fieldItem.value){
//                                                                    var currentOrderType = fieldItem.value[key4].toLowerCase(); 
//                                                                    console.log("subItem[key2]"); 
//                                                                    console.log(subItem[key2]); 
//                                                                    console.log(fieldItem); 
//                                                                    
//                                                                    if(currentOrderType == (_orderStatus[1]).toLowerCase() )
//                                                                    {
//                                                                        matchSuccessSrc = true;
////                                                                        console.log(fieldItem + " compare " +(_orderStatus[0]).toLowerCase()); 
//                                                                       } 
//                                                                }
                                                                        matchSuccessSrc = true;
                                                        break;
                                                        
                                                    case 2:// show order without APDI, PDI or PRE PDI  i.e. APS orders
//                                                           console.log(fieldItem.value);
                                                                for(var key4 in fieldItem.value){
                                                                    var currentOrderType = fieldItem.value[key4].toLowerCase();
                                                                    
                                                                    if( ( currentOrderType != (_orderStatus[0]).toLowerCase()) ||
                                                                        ( currentOrderType != (_orderStatus[1]).toLowerCase()) ||
                                                                        ( currentOrderType != (_orderStatus[2]).toLowerCase()) ){
                                                                            matchSuccessSrc = true;
//                                                                        console.log(fieldItem); 
                                                                       } 
                                                                }
                                                         break;
                                                        
                                                    case 3:// show all orders
//                                                                for(var key4 in fieldItem.value){
//                                                                    var currentOrderType = fieldItem.value[key4].toLowerCase();
//                                                                    
//                                                                    if( ( currentOrderType != (_orderStatus[0]).toLowerCase()) ||
//                                                                        ( currentOrderType != (_orderStatus[1]).toLowerCase()) ||
//                                                                        ( currentOrderType != (_orderStatus[2]).toLowerCase()) ){
////                                                                       
////                                                                        console.log(fieldItem); 
//                                                                       } 
//                                                                }
                                                         matchSuccessSrc = true;
                                                         break;
                                                        
                                                 }
                                                }
									
									//orderType
												if(fieldItem.name== "orderType"){ 
                                                    
                                                     switch(__pdiSelection){
                                                                    
                                                        case 0:   //PPDI order

                                                        for(var key4 in fieldItem.value){	                             
                                                                    if(fieldItem.value[key4] ==  _orderStatus[0]  ){                    
//                                                            console.log(fieldItem.value[key4]  +" "+ _orderStatus[0] +" "+ orderID );
//                                                                        console.log(fieldItems);
    																	matchSuccessTpe= true;
                                                                    //	console.log(fieldItem.displayValue[key4]);
                                                                    }
                                                        } 
                                                                   break; 
                                                        case 1:   //PDI order

                                                        for(var key4 in fieldItem.value){	                                                
//                                                            console.log(fieldItem.value[key4]  +" "+ _orderStatus[1] );
                                                                    if(fieldItem.value[key4] ==  _orderStatus[1] ){ 
    																	matchSuccessTpe= true;
                                                                    //	console.log(fieldItem.displayValue[key4]);
                                                                    }
                                                        } 
                                                                   break; 
                                                             
                                                        case 2:   //APS order

                                                        for(var key4 in fieldItem.value){	                                                
//                                                            console.log(fieldItem.value[key4]  +" "+ _orderStatus[0] );
                                                                    if(fieldItem.value[key4] !=  _orderStatus[0] && 
                                                                      fieldItem.value[key4] !=  _orderStatus[1] && 
                                                                      fieldItem.value[key4] !=  _orderStatus[2] ){ 
    																	matchSuccessTpe= true;
                                                                    //	console.log(fieldItem.displayValue[key4]);
                                                                    }
                                                        } 
                                                                   break; 
                                                             
                                                             
                                                        case 3:   //APS order
                                                                matchSuccessTpe= true;
                                                                   break; 
                                                     }
                                                    
                                                    
                                                    
												}
												
											}			 
									} 
									if(matchSuccessTpe && matchSuccessSrc ){
//                                         
//											for(var key3 in subItem[key2].field){ 
//												var fieldItem = fieldItems[key3];
//												if(fieldItem.name== "orderSource"){  
////                                                        console.log(fieldItem.value);
//                                                } 
//                                            }
                                         
                                        
                                        orderIDs.push(orderID);}	
//									if(matchSuccessSrc ){orderIDs.push(orderID);}									
								 } 
							 } 
						}catch(err){
							 console.log("error : "+err);
						}
						
						//show how many found so far
						if(orderIDs.length < 501){
							$('#basic-addon2').text('found ('+orderIDs.length+')');
						}else{
							$('#basic-addon2').text('');
						}
						
						//  console.log(orderIDs.length);  
						paginateAndLoadOrder(orderIDs);  

//                        $('#result').addClass('hidden');						
				  
					}).fail(function (result) { 
//                            resultDiv.text(result);  
                        console.log(result);
                             showHideAlertMessage("error loading",false, 200);
                    }); 
                    }
                    
                    else{
				 	kV.getAllOrdersList(_query,_sortby, _indexBy, __pdiSelection).done(function (result) { 
                //   kV.multiPageSearchRequest(_query,_sortby, _indexBy, _from, _to).done(function (result) {  
						var orderIDs = [];
                        
						try{
							 var myRecords = JSON.parse(result);
//							 console.log(myRecords);   
                             
							 for(var key in myRecords){  
								var subItem = myRecords[key].item;  
								for(var key2 in subItem){    
									var matchSuccessSrc = false;
									var matchSuccessTpe = false; 
									var orderID = subItem[key2].id;  
                                    
                                    
									if (orderID.match(/^\d+$/) && orderID != null){  
										var fieldItems = subItem[key2].field;
											for(var key3 in fieldItems){ 
												var fieldItem = fieldItems[key3];
										//		console.log("fieldItem "+fieldItem);
										//	console.log(fieldItem); 								
												
									//orderSource not pdi
//												if(fieldItem.name== "orderSource"){ 
//													for(var key4 in fieldItem.displayValue){	
//																if(((fieldItem.displayValue[key4]).toLowerCase() != (_orderStatus[2]).toLowerCase())
//																|| ((fieldItem.displayValue[key4]).toLowerCase() != (_orderStatus[0]).toLowerCase())
//																||  ((fieldItem.displayValue[key4]).toLowerCase() != (_orderStatus[1]).toLowerCase()) ){ 
//																	matchSuccessSrc = true;
//																//	console.log(fieldItem.displayValue[key4]);
//																}
//													} 
//												}
                                                
                                            if(fieldItem.name== "orderSource"){ 
                                                
//												console.log(fieldItem.value);
//                    console.log("_pdiSelection "+ __pdiSelection);
                                                
//                                                var _orderStatus = ["PPDI", "PDI", "APDI"]; 
                                                switch(__pdiSelection){
                                                    case 0: // show pre pdi   
//                                                            console.log(fieldItem.value);
                                                                for(var key4 in fieldItem.value){
                                                                    var currentOrderType = fieldItem.value[key4].toLowerCase(); 
//                                                                    console.log(currentOrderType +" " +(_orderStatus[0]).toLowerCase()); 
                                                                    if(currentOrderType == (_orderStatus[0]).toLowerCase() )
                                                                    {
                                                                        matchSuccessSrc = true;
//                                                                        console.log(fieldItem + " compare " +(_orderStatus[0]).toLowerCase()); 
                                                                       } 
                                                                }
                                                        break;
                                                        
                                                    case  1: ///show only pdi //order type pdi
//                                                            console.log(fieldItem.value);
                                                                for(var key4 in fieldItem.value){
                                                                    var currentOrderType = fieldItem.value[key4].toLowerCase(); 
//                                                                    console.log(currentOrderType +" " +(_orderStatus[0]).toLowerCase()); 
                                                                    if(currentOrderType == (_orderStatus[1]).toLowerCase() )
                                                                    {
                                                                        matchSuccessSrc = true;
//                                                                        console.log(fieldItem + " compare " +(_orderStatus[0]).toLowerCase()); 
                                                                       } 
                                                                }
                                                        break;
                                                        
                                                    case 2:// show order without APDI, PDI or PRE PDI  i.e. APS orders
/////// ///also order type valid
//                                                           console.log(fieldItem.value);
                                                                for(var key4 in fieldItem.value){
                                                                    var currentOrderType = fieldItem.value[key4].toLowerCase();
                                                                    
                                                                    if( ( currentOrderType != (_orderStatus[0]).toLowerCase()) ||
                                                                        ( currentOrderType != (_orderStatus[1]).toLowerCase()) ||
                                                                        ( currentOrderType != (_orderStatus[2]).toLowerCase()) ){
                                                                            matchSuccessSrc = true;
//                                                                        console.log(fieldItem); 
                                                                       } 
                                                                }
                                                         break;
                                                        
                                                    case 3:// show all orders 
                                                         matchSuccessSrc = true;
                                                         break;
                                                        
                                                 }
                                                }
									 
											}			 
									} 
								//	if(matchSuccessTpe && matchSuccessSrc ){orderIDs.push(orderID);}	
									if(matchSuccessSrc ){orderIDs.push(orderID);}	
                                        
                                       
								 } 
                                    
							 } 
						}catch(err){
							 console.log("error : "+err);
						}
						
						//show how many found so far
						if(orderIDs.length < 501){
							$('#basic-addon2').text('found ('+orderIDs.length+')');
						}else{
							$('#basic-addon2').text('');
						}
						
						//  console.log(orderIDs.length);  
						paginateAndLoadOrder(orderIDs);  

//                        $('#result').addClass('hidden');						
				  
                    showHideMainLoader(false);
					}).fail(function (result) { 
//                            resultDiv.text(result);  
                                console.log(result);
                             showHideAlertMessage("error loading",false, 200);
                        showHideMainLoader(false);
                    }); 
                    }
				}
				 	
				function paginateAndLoadOrder(orderIDs){
						//setting pagination variables
						list = orderIDs;
						numPerPage = Number( $("#listofOrders").val()) ;
						numberOfPages =  Math.ceil(orderIDs.length / numPerPage);   // calculates the total number of pages
 
					//	console.log("totalPageNum " + numberOfPages +" numPerPage " + numPerPage);
						
					try{
						loadOrderList();
					}catch(err){
						console.log("err 11313 " +err);
					}
				}
				
				function loadOrderList() {
					//clear table content
					$("#ordertbodyid").empty();
					 
					var begin = ((currentPage - 1) * numPerPage);
					
					var end = begin + numPerPage;
					
				//	console.log("begin "+ begin +" end "+end);

					pageList = list.slice(begin, end);
					
				//	console.log("pageList");
				//	console.log(pageList.length);
					
					drawList();    // draws out our data
					check();         // determines the states of the pagination buttons
				}
				function drawList() {  
						showHideMainLoader(true);
						LoadOrderByIds(pageList, numPerPage);
				}
				function check() {
					document.getElementById("next").disabled = currentPage == numberOfPages ? true : false;
					document.getElementById("previous").disabled = currentPage == 1 ? true : false;
					document.getElementById("first").disabled = currentPage == 1 ? true : false;
					document.getElementById("last").disabled = currentPage == numberOfPages ? true : false;
				}
				function nextOrderPage() {
					currentPage += 1;
					loadOrderList();
				}
				function previousOrderPage() {
					currentPage -= 1;
					loadOrderList();
				}
				function firstOrderPage() {
					currentPage = 1;
					loadOrderList();
				}
				function lastOrderPage() {
					currentPage = numberOfPages;
					loadOrderList();
				}
 
//call KV get order by Id then call parse 
				function LoadOrderByIds(orderIDs, noOfOrdertoShow){
					//	console.log("loading order by ids " +orderIDs.length + " noOfOrdertoShow "+  noOfOrdertoShow);
						var loopElement =0; 
						function loadOrderElement(){ 
							if (loopElement ==  orderIDs.length  || loopElement ==  noOfOrdertoShow ) { 
						//		console.log("loading order by length " +orderIDs.length + " noOfOrdertoShow "+  noOfOrdertoShow +" ");
						//		console.log("breaked "+loopElement +" incrementtest "+ incrementtest); 
								showHideMainLoader(false);
								return;
							} 
							try {
								kV.GetOrderById(orderIDs[loopElement] ).done(function (orderResult) { 
								//			console.log("loopElement "+orderIDs[loopElement] );
																	var orderJSON = JSON.parse(orderResult); 
																	//	console.log("orderID from server:"+orderJSON.order.orderId );
																	//	console.log(orderJSON); 
																	
																	//adding current orrder to the listview
																	parseAndFilterOrderThenAdd(orderJSON.order);
																	 
																	loopElement++;
																	loadOrderElement();
																	
																}).fail(function (err) {  
																//	console.log("error get order by ID : "+err); 
																	loopElement++;
																	loadOrderElement();
																});
									 
								}catch(err){
							//		 console.log("error : "+err);
								}
						} 
						
						loadOrderElement();
					
				}
			 	
			//need to add ship to address from customer and customer info from order
				function parseAndFilterOrderThenAdd(order){  
												//					console.log("parseandFilter "+order.orderId); 
													//				console.log(order); 
																	//for pdi selection house type
													/* done */		var Housedescription ="";
																	  
																	//for pdi preview 
																	var unitEnrollmentNum ="";
																	//order delivery date
													/* done */		var dateOfPossession = Date.now();
																	try{
																		 dateOfPossession = order.deliveryDate;
																	} catch(err){  }
																	
																	 
																	var vendorRef ="";
																//only available for Brick and oak, 
																//fill if info avaiable  either condo or municipality	
																	var lotNo ="";
																	var PlanNo ="";
																	var Municipality =""; 
																//or	
																//only available for Brick and oak, 
																	var condoProjectName ="";
																	var condoLevel ="";
																	var condoUnit ="";
																	
																	//lot description
													/* done */		var address ="";
																	//catalog name
													/* done */		var vendorName ="";
																	
													/* done */		var representativeName ="";
													/* done */		var purchaserName1 =  order.customerInfo.customerName;
													/* done */ 		var purchaserName2 ="";
													/* done */		var DesignateName ="";
														
													

													//json stringify customer info and send that to creatOrder	
													//also copy the partyRoleInfo from the order
																	//get order party names
																	try{
																		if(order.partyRoleInfo.length > 0){
																			for(var  _cust = 0 ; _cust < order.partyRoleInfo.length; _cust++ ){
																				var roleType= order.partyRoleInfo[_cust].orderPartyRole;
																				
																				if(roleType=="CUSTOMER"  ){
																					purchaserName1 = purchaserName1+", " + order.partyRoleInfo[_cust].partyName; 
																				} 
																				
																				if(roleType=="REPRESENTATIVE"  ){
																					representativeName = representativeName + order.partyRoleInfo[_cust].partyName; 
																				} 
																				if(roleType=="PURCHASER"  ){
																					if(purchaserName2 ==""){
																						purchaserName2 =  order.partyRoleInfo[_cust].partyName;
																					}else{
																						purchaserName2 = purchaserName2 +", "+ order.partyRoleInfo[_cust].partyName;
																					}
																				}
																				
																				if(roleType=="DESIGNATE"  ){
																					if(DesignateName ==""){
																						DesignateName =  order.partyRoleInfo[_cust].partyName;
																					}else{
																						DesignateName = DesignateName +", "+ order.partyRoleInfo[_cust].partyName;
																					}
																				}
																			}
																		} 
																	}catch(err){
																	//	console.log("err party role getting "+err);
																		}
																	 
																	 //for test now
																	var isForTarion = false;
																	
																	var _products = order.product;
																	try{
																	if(_products.length > 0){
																		for(var  _product = 0 ; _product < _products.length; _product++ )	{ 
																		 
																			// console.log(_products[_product]);					 
																		//to filter all items
																		
																		//brick and Oak
																		if((_products[_product].productCode).startsWith("LOT") && (_products[_product].productCatalog).startsWith("KettleBeck")){
																			if(_products[_product].configuration){ 
																			//	console.log( _products[_product]);
																				//mark true to add to select list
																				 isForTarion = true;
																				 
																				unitEnrollmentNum = _products[_product].configuration.unitEnrollment;
																				
																				//lot description is address
																				vendorRef = _products[_product].configuration.vendorBuilderReferenceNum;
																				
																				lotNo = _products[_product].configuration.lotNumber;
																				PlanNo = _products[_product].configuration.planNum;
																				Municipality = _products[_product].configuration.municipality; 
																				
																				condoProjectName = _products[_product].configuration.condominiumProjectName;
																				condoLevel = _products[_product].configuration.Condolevel;
																				condoUnit = _products[_product].configuration.Condounit; 
																				 
																				 if(_products[_product].configuration.homeCivicAddress){
																					address = _products[_product].configuration.homeCivicAddress;
																				 }
																				 
																				vendorName =  _products[_product].configuration.vendorBuilderName;
																			}
																		}
																		//brick and Oak
																		else if((_products[_product].productCode).startsWith("T")  && (_products[_product].productCatalog).startsWith("KettleBeck")){
																			//mark true to add to select list
																			isForTarion = true;
																			Housedescription = _products[_product].description; 
																		}
																		//preston
																		else if((_products[_product].productCode).startsWith("LOT") && ((_products[_product].productCatalog).startsWith("preston_park") ||(_products[_product].productCatalog).startsWith("prestonPark") )){
																			
																			if(_products[_product].configuration){ 
																			//	console.log( _products[_product]);
																				//mark true to add to select list
																				 isForTarion = true;
																				 
																				unitEnrollmentNum = _products[_product].configuration.unitEnrollment;
																				
																				//lot description is address
																				vendorRef = _products[_product].configuration.vendorBuilderReferenceNum;
																				
																				lotNo = _products[_product].configuration.lotNumber;
																				PlanNo = _products[_product].configuration.planNum;
																				Municipality = _products[_product].configuration.municipality; 
																				
																				condoProjectName = _products[_product].configuration.condominiumProjectName;
																				condoLevel = _products[_product].configuration.Condolevel;
																				condoUnit = _products[_product].configuration.Condounit; 
																				 
																				 if(_products[_product].configuration.homeCivicAddress){
																					address = _products[_product].configuration.homeCivicAddress;
																				 }
																				 
																				vendorName =  _products[_product].configuration.vendorBuilderName;
																			}
																		}
																		//preston
																		else if((_products[_product].productCode).startsWith("T") && ((_products[_product].productCatalog).startsWith("preston_park") ||(_products[_product].productCatalog).startsWith("prestonPark") )){
																			isForTarion = true;
																			Housedescription = _products[_product].description; 
																		}
																		//abelie
																		else if((_products[_product].productCode).startsWith("BND") && ((_products[_product].productCatalog).startsWith("abeliecondos")   )){
																			isForTarion = true;
																			//console.log(_products[_product]); 
																			if(_products[_product].bundleItem){ 
																				var bundleItems = _products[_product].bundleItem; 
																				for(var  _productbundleItem = 0 ; _productbundleItem < bundleItems.length; _productbundleItem++ )	{ 
																						var bundlesingleItem = bundleItems[_productbundleItem]; 
																							if(bundlesingleItem.productCode.startsWith("T-")){  
																						//		console.log(order.orderId);
																								Housedescription = bundlesingleItem.description;  
																							} 
																						}  
																			}
																			//get data from configuration 
																			if(_products[_product].configuration){ 	 
																					unitEnrollmentNum = _products[_product].configuration.unitEnrollment;
																					//lot description is address
																					vendorRef = _products[_product].configuration.vendorBuilderReferenceNum;																		
																					lotNo = _products[_product].configuration.lotNumber;
																					PlanNo = _products[_product].configuration.planNum;
																					Municipality = _products[_product].configuration.municipality; 
																					
																					condoProjectName = _products[_product].configuration.condominiumProjectName;
																					condoLevel = _products[_product].configuration.Condolevel;
																					condoUnit = _products[_product].configuration.Condounit; 
																					 
																					 if(_products[_product].configuration.homeCivicAddress){
																						address = _products[_product].configuration.homeCivicAddress;
																					 }
																					 
																					vendorName =  _products[_product].configuration.vendorBuilderName;
																				
																			} 
																		}
																		else if((_products[_product].productCode).startsWith("BND") && ((_products[_product].productCatalog).startsWith("burlington")   )){
																				//mark true to add to select list
																				 isForTarion = true;
																				if(_products[_product].bundleItem){ 
																					var bundleItems = _products[_product].bundleItem; 
																					for(var  _productbundleItem = 0 ; _productbundleItem < bundleItems.length; _productbundleItem++ )	{ 
																							var bundlesingleItem = bundleItems[_productbundleItem]; 
																								if(bundlesingleItem.productCode.startsWith("T-")){  
																							//		console.log(order.orderId);
																									Housedescription = bundlesingleItem.description;  
																								} 
																							}  
																				}
																			//get data from configuration 
																			if(_products[_product].configuration){ 	 
																					unitEnrollmentNum = _products[_product].configuration.unitEnrollment;
																					//lot description is address
																					vendorRef = _products[_product].configuration.vendorBuilderReferenceNum;																		
																					lotNo = _products[_product].configuration.lotNumber;
																					PlanNo = _products[_product].configuration.planNum;
																					Municipality = _products[_product].configuration.municipality; 
																					
																					condoProjectName = _products[_product].configuration.condominiumProjectName;
																					condoLevel = _products[_product].configuration.Condolevel;
																					condoUnit = _products[_product].configuration.Condounit; 
																					 
																				 if(_products[_product].configuration.homeCivicAddress){
																					address = _products[_product].configuration.homeCivicAddress;
																				 }
																					 
																					vendorName =  _products[_product].configuration.vendorBuilderName;
																				
																			} 

																		}
																		//show other orders even if there is no matching product
																		else{
																			isForTarion = true;
																			address = "--Not valid--";
																		}																	
																				 
																		}
																		//for loop ends here
																		}
																		//if empty product cart
																		else{
																			isForTarion = true;
																			address = "--Empty--";
																			
																		}
																	}catch(err){
																	//	console.log("err in product parsing "+err);
																		}
																	
																	
//                    debugger;
																	if(isForTarion){ 
																//		console.log(order.orderId);
															//		console.log(order);
																	
																var singleOrderData = new  OrderData2 (order.orderId,
																		order.__metadata.displayName,
																		Housedescription,
																		order.createdDate,
																		purchaserName1,
																		address,
																		unitEnrollmentNum,
																		dateOfPossession,
																		vendorRef,
																		lotNo,
																		PlanNo,
																		Municipality,
																		condoProjectName,
																		condoLevel,
																		condoUnit,
																		vendorName,
																		representativeName, 
																		purchaserName2,
																		DesignateName
																		//,JSON.stringify(order)
																		, order 
																		);
																		
																		//adding whole order to the array for fetching later
															//			console.log("adding id "+ order.orderId); 
																		addOrderToList(singleOrderData); 
																	}
                    
//                    debugger;
					
				}
			 	
				var incrementtest = 1;
			
	//appending order data to selecting order view
				function addOrderToList(_order){ 
				
//					console.log("getting in add ");
//					console.log(_order);
				//	console.log(ListOrderStorage[_order.id]);
//					console.log("getting in add ends ");
//					console.log($("#ordersListed table tbody"));
					
				$("#ordersListed table tbody").append( 
																				'<tr style=\"cursor: pointer; cursor: hand;\" id=\'tr_'+_order.id+'\' data-id=\"'+ _order.id +'\"><td>'+
																				//	'<input class=\"orderDat subItemlowColor hidden\" value=\''+
																				//		JSON.stringify(_order)+
																				//	'\'></input>'+ 
																				//+incrementtest+" "
																					'<div  class=\"subItemlowColor\">'+_order.orderdisplayName+'</div>'+
																					'<div><span class=\"subItemlowColor\">House type</span> '+ 
																					_order.houseType
																					+'</div>'+
																					'<div><span  class=\"order_date\">'+
                                                                                     moment(_order.datecreated).format("YYYY-MM-DD hh:mm a")+'</span></div>' +
																					'</td><td>'+
																					'<div class=\"name\">'+ _order.customerName+
																					'</div>'+
																				'</td><td>'+
																					'<div>'+
																					_order.addressofInspectedHouse+
																					'</td><td class="hidden-xs hidden-sm">'+
																					'<span id=\''+_order.id+'\' type=\'button\' class=\'itemSelect  '+
																					' \' aria-label=\'Left Align\'>'+
																				     '<span class=\'glyphicon glyphicon-chevron-right\' ></span>'+
																					 '</span>'+
																				'</td></tr>'
																				);
				//saving whole order to retieve later
					$( "#tr_"+_order.id ).data( "wholeOrderStorage", _order  );
				//	console.log($( "#tr_"+_order.id ).data( "wholeOrderStorage" )); 
				
				incrementtest++;
				}
	



//order selection ends here