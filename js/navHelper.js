var basil_nav ;
var basil_nav_options = { namespace: usrNameSpace };


// function for push menu						
    function pushMenu(){
	
	(function(window) {

  'use strict';

  /**
   * Extend Object helper function.
   */
  function extend(a, b) {
    for(var key in b) { 
      if(b.hasOwnProperty(key)) {
        a[key] = b[key];
      }
    }
    return a;
  }

  /**
   * Each helper function.
   */
  function each(collection, callback) {
    for (var i = 0; i < collection.length; i++) {
      var item = collection[i];
      callback(item);
    }
  }

  /**
   * Menu Constructor.
   */
  function Menu(options) {
    this.options = extend({}, this.options);
    extend(this.options, options);
    this._init();
  }

  /**
   * Menu Options.
   */
  Menu.prototype.options = {
    wrapper: '#o-wrapper',          // The content wrapper
    type: 'slide-left',             // The menu type
    menuOpenerClass: '.c-button',   // The menu opener class names (i.e. the buttons)
    maskId: '#c-mask'               // The ID of the mask
  };

  /**
   * Initialise Menu.
   */
  Menu.prototype._init = function() {
    this.body = document.body;
    this.wrapper = document.querySelector(this.options.wrapper);
    this.mask = document.querySelector(this.options.maskId);
    this.menu = document.querySelector('#c-menu--' + this.options.type);
    this.closeBtn = this.menu.querySelector('.c-menu__close');
    this.menuOpeners = document.querySelectorAll(this.options.menuOpenerClass);
    this._initEvents();
  };

  /**
   * Initialise Menu Events.
   */
  Menu.prototype._initEvents = function() {
    // Event for clicks on the close button inside the menu.
    this.closeBtn.addEventListener('click', function(e) {
      e.preventDefault();
      this.close(); 
    }.bind(this));

    // Event for clicks on the mask.
    this.mask.addEventListener('click', function(e) {
      e.preventDefault();
      this.close(); 
    }.bind(this));
	
    // Event for clicks on the mask.
    this.menu.addEventListener('click', function(e) {
      e.preventDefault();
      this.close(); 
    }.bind(this));
	
  };

  /**
   * Open Menu.
   */
  Menu.prototype.open = function() {
    this.body.classList.add('has-active-menu');
    this.wrapper.classList.add('has-' + this.options.type);
    this.menu.classList.add('is-active');
    this.mask.classList.add('is-active');
    this.disableMenuOpeners();
  };

  /**
   * Close Menu.
   */
  Menu.prototype.close = function() {
    this.body.classList.remove('has-active-menu');
    this.wrapper.classList.remove('has-' + this.options.type);
    this.menu.classList.remove('is-active');
    this.mask.classList.remove('is-active');
    this.enableMenuOpeners();
  };

  /**
   * Disable Menu Openers.
   */
  Menu.prototype.disableMenuOpeners = function() {
    each(this.menuOpeners, function(item) {
      item.disabled = true;
    });
  };

  /**
   * Enable Menu Openers.
   */
  Menu.prototype.enableMenuOpeners = function() {
    each(this.menuOpeners, function(item) {
      item.disabled = false;
    });
  };

  /**
   * Add to global namespace.
   */
  window.Menu = Menu;

})(window);
	
	
		 /**
   * Push left instantiation and action.
   */
  var pushLeft = new Menu({
    wrapper: '#o-wrapper',
    type: 'push-left',
    menuOpenerClass: '.c-button',
    maskId: '#c-mask'
  });

  var pushLeftBtn = document.querySelector('#c-button--push-left');
  
  pushLeftBtn.addEventListener('click', function(e) {
    e.preventDefault;
    pushLeft.open();
  });

	
    
    
    
    loadNavBtns();
}

//buttons functality for nav
    function loadNavBtns(){
         
        //go to home switch class    
          $(".NavHome").unbind().click(function () {
                    if(!$('#generateOrderp_Pdi').hasClass( "hidden" )){
                            $('#generateOrderp_Pdi').addClass('hidden');  
                    } 
              
                            switchUI(1);  
           }); 
 
   //logout button
            $("#logoutbtn").unbind().click(function () {
                switchUI(0); 
                clearLocalUserCache();
                kV.logout().done(function (result) {
                        //	console.log(result); 
                        showHideAlertMessage("Signed out successfully", true, 300); 
                }).fail(function (result) {   
                        //	console.log("err "+result);
                        showHideAlertMessage("Signed out failed", false, 300); 
                //			$('#result').removeClass('hidden').text(" pre pdi generate failed");
                });   
            });  
    //nav bar active ui switch    
        $(".main_Navbar").click(function () {
                $(".main_Navbar").removeClass("active");
                // $(".tab").addClass("active"); // instead of this do the below 
                $(this).addClass("active");   
            });
        
        
		loadUserInfo();		
}


//clear local variables or cache
function clearLocalUserCache(){  
    var basil = new window.Basil(basil_nav_options);
    //reset only user namespace
    basil.reset(basil_nav_options);  
} 

 //change password        
          function changePassword(){ 
						var old = $('input[name="oldpassword"]').val();
						var New = $('input[name="newpassword"]').val();
						var Retype = $('input[name="Retypepassword"]').val();
                              if(old.length > 0  &&  New.length > 0  &&  Retype.length > 0){
                                        if(New != Retype){
                                            alert('new password does not match');
                                            return;
                                        }
                                        if(old == Retype){
                                            alert('Cannot repeat same Password');
                                            return;
                                        }
                                        kV.changePassword(old,New,Retype).done(function (result) {
                                                $('#passwordResetModal').modal('hide');
                                        }).fail(function (result) {  
                                            if(result == "Invalid user/password"){
                                                alert('Invalid Old Password');
                                            }else{
                                                alert(result);
                                            }
                                            $('#passwordResetModal').modal('hide');
                                        }); 
                                     }
	 }

//function

//loading user info from the kv environment
function loadUserInfo(){  
        basil_nav = new window.Basil(basil_nav_options);
        var userfname = basil_nav.get("firstName");  
        var userlname = basil_nav.get("lastName");

        var usrName = userfname +" " +userlname; 
    //	$("#userFLname").html(usrName);
        $(".userFlNameHere").html(usrName);

    //    console.log("userlname "+$("#userFLname").val());

    //    debugger;

        var userStatus = basil_nav.get("status");
        $("#userStatusLabel").html(userStatus ); 
//        console.log(userStatus);
    //    debugger;

        $("userStatusLabel").data("userId", basil_nav.get("userId"));
        $("userStatusLabel").data("language", basil_nav.get("language"));  
    
}

//saving clicked item to cart			
	function addItemToCart(item){
//					console.log('item added to cart');
//					$('.houseCartForStorage').removeClass('hidden');
//					console.log(item);
					$('#cart_house_Id').text(item.orderdisplayName); 
					$('#cart_house_Id').data( "wholeOrderStored", item  );
					$('#cart_cust_name').text(item.customerName); 
		}


//function to clear cart
   function ClearCartItems(){ 
					$('#cart_house_Id').text(""); 
					$('#cart_house_Id').data(""); 
					$('#cart_cust_name').text("");
   }