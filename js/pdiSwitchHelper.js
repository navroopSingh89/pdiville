var EditingOrder = false;

//init function for pdi selector
function loadPDISelectorUI(){
     EditingOrder = false;
//    console.log("loaded pdi");
    //hide nav totally 
      $(".main_Navbar").addClass("hidden");
//      $(".pdi_Navbar").removeClass("hidden");
    
    loadPdiNavbar();
    //loading selected order data from the cart order id
//					$('#cart_house_Id').data( "wholeOrderStored", item  );
     var wholeorder_tr_data =    $('#cart_house_Id').data("wholeOrderStored"); 
    
//    console.log("wholeorder_tr_data" );
//    console.log(wholeorder_tr_data );
    
      loadPDIchooser(wholeorder_tr_data);
}

//show only as switch and make it disabled on current page
function loadPdiNavbar(){ 
    $(".pdi_Navbar").addClass("hidden");
   $('#topNavBar > div > div.navbar-header.navbar-default > ul > li:nth-child(6)').addClass('active disabled').removeClass("hidden");
    
    
}
//choose between pre pdi or pdi 		 
		 function loadPDIchooser(order_tr_data){  
			//reset array for loading data
			issuesArrList = new Array(); 
			
			//filter pdi or pre pdi selector here 
			var orderMain_ = (order_tr_data.FullOrder);
             
//             console.log(orderMain_);
               //if selected order is PPDI
                if(orderMain_.orderType == _orderStatus[0]){
                    console.log("PPDI order selected");
//                         showHideMainLoader(false);
//                    console.log("loading data to issues table");
                        EditingOrder = true; 
                        switchPrePdiOrPDI(true, order_tr_data); 
                }
                //If selected order is PDI
                else if(orderMain_.orderType == _orderStatus[1]){
                        console.log("PDI order selected"); 
//                        console.log("loading data to issues table");
                        EditingOrder = true; 
                        switchPrePdiOrPDI(false, order_tr_data); 
                }
             //any other order can choose
             else{

                //console.log(orderMain_.orderSource);
                        switchChooserUIbasedonOrder(orderMain_); 

                        // console.log(order_tr_data);
                         $('#ppdibtn').unbind().click(function(){
                             //prePDIselected used to switch generate button function 
                                switchPrePdiOrPDI(true, order_tr_data); 
                        }); 

                         $("#pdibtn").unbind().click(function(){
                                switchPrePdiOrPDI(false, order_tr_data);  
                        });  
                }
                        
             
//			//console.log(orderMain_.orderSource);
//			switchChooserUIbasedonOrder(orderMain_); 
//			 
//			// console.log(order_tr_data);
//			 $('#ppdibtn').unbind().click(function(){
//				 //prePDIselected used to switch generate button function 
//					switchPrePdiOrPDI(true, order_tr_data); 
//			}); 
//			 
//			 $("#pdibtn").unbind().click(function(){
//					switchPrePdiOrPDI(false, order_tr_data);  
//			});  
		 } 
		 
	//hide or show pre pdi or pdi button based on order
		 function switchChooserUIbasedonOrder(orderMain_){ 
			//if  order source doesnot exists then show pre pdi and pdi button
			if(orderMain_.orderSource){
		//	console.log("yes order_tr_data " +orderMain_.orderSource);
				//if pdi has already been generated then donot show pdi button
				if((orderMain_.orderSource).toLowerCase() == (_orderStatus[0]).toLowerCase() ){
			//			console.log("yes  pp order_tr_data");
                    
                    showHideAlertMessage("Pre PDI has been already generated for this order",true, 200);
//					$('#result').removeClass('hidden').text("Pre PDI has been already generated for this order");
					if(!$('#ppdibtn_wrapper').hasClass( "hidden" )){
						$('#ppdibtn_wrapper').addClass('hidden');  
					}
//                    if(!$('#ppdibtn').hasClass( "hidden" )){
//						$('#ppdibtn').addClass('hidden');  
//					}
					if($('#pdibtn_wrapper').hasClass( "hidden" )){
						$('#pdibtn_wrapper').removeClass('hidden');    
					}
//					if($('#pdibtn').hasClass( "hidden" )){
//						$('#pdibtn').removeClass('hidden');  
//					}
                    
					if(!$('#all_wrapper').hasClass( "hidden" )){
						$('#all_wrapper').addClass('hidden');  
					}
                    
					$('#pdibtn_wrapper').addClass('col-lg-12');  
                    $('#pdibtn_wrapper').removeClass('col-lg-6');
				} 
				//double check if pdi has been made no continue for pdi process
				else if ((orderMain_.orderSource).toLowerCase() == (_orderStatus[1]).toLowerCase() ){
					//	console.log("yes  p order_tr_data");
//					if($('#ppdibtn').hasClass( "hidden" )){
//						$('#ppdibtn').removeClass('hidden');  
//					}
                    if($('#ppdibtn_wrapper').hasClass( "hidden" )){
						$('#ppdibtn_wrapper').removeClass('hidden');  
					}
//					if(!$('#pdibtn').hasClass( "hidden" )){
//						$('#pdibtn').addClass('hidden');  
//					}
					if(!$('#pdibtn_wrapper').hasClass( "hidden" )){
						$('#pdibtn_wrapper').addClass('hidden');  
					} 
					if(!$('#all_wrapper').hasClass( "hidden" )){
						$('#all_wrapper').addClass('hidden');  
					}
                    
					$('#ppdibtn_wrapper').addClass('col-lg-12');  
                    $('#ppdibtn_wrapper').removeClass('col-lg-6');
                    
                    showHideAlertMessage("PDI has been already generated for this order. ",true, 200);
//					$('#result').removeClass('hidden').text("PDI has been already generated for this order. ");
				}
				//double check if all pdi then show nothing as pre pdi or pdi has been generated 
				else if ((orderMain_.orderSource).toLowerCase() == (_orderStatus[1]).toLowerCase() ){
				//		console.log("yes  p order_tr_data");
//					if(!$('#ppdibtn').hasClass( "hidden" )){
//						$('#ppdibtn').addClass('hidden');  
//					}
//					if(!$('#pdibtn').hasClass( "hidden" )){
//						$('#pdibtn').addClass('hidden');  
//					} 
                    if(!$('#ppdibtn_wrapper').hasClass( "hidden" )){
						$('#ppdibtn_wrapper').addClass('hidden');  
					}
                    if(!$('#pdibtn_wrapper').hasClass( "hidden" )){
						$('#pdibtn_wrapper').addClass('hidden');  
					} 
					if($('#all_wrapper').hasClass( "hidden" )){
						$('#all_wrapper').removeClass('hidden');  
					}
                    showHideAlertMessage("PDI and PRE PDI  has been already generated for this order. So cannot go back to generate",true, 200);
//					$('#result').removeClass('hidden').text("PDI and PRE PDI  has been already generated for this order. So cannot go back to generate");
				}
				//if no pdi or pre pdi has been generated then show both buttons
				else{ 
//					if($('#ppdibtn').hasClass( "hidden" )){
//						$('#ppdibtn').removeClass('hidden');  
//					}
//					if($('#pdibtn').hasClass( "hidden" )){
//						$('#pdibtn').removeClass('hidden');  
//					} 
                    if($('#ppdibtn_wrapper').hasClass( "hidden" )){
						$('#ppdibtn_wrapper').removeClass('hidden');  
					}
                    if($('#pdibtn_wrapper').hasClass( "hidden" )){
						$('#pdibtn_wrapper').removeClass('hidden');  
					} 
                    
					if($('#all_wrapper').hasClass( "hidden" )){
						$('#all_wrapper').addClass('hidden');  
					}
				}  
			}else{ 
                    if($('#ppdibtn_wrapper').hasClass( "hidden" )){
						$('#ppdibtn_wrapper').removeClass('hidden');  
					}
                    if($('#pdibtn_wrapper').hasClass( "hidden" )){
						$('#pdibtn_wrapper').removeClass('hidden');  
					} 
//					if($('#ppdibtn').hasClass( "hidden" )){
//						$('#ppdibtn').removeClass('hidden');  
//					}
//					if($('#pdibtn').hasClass( "hidden" )){
//						$('#pdibtn').removeClass('hidden');  
//					} 
					if($('#all_wrapper').hasClass( "hidden" )){
						$('#all_wrapper').addClass('hidden');  
					}
			}
		 }
	

		 
//switch between pdi or prepdi load
		 function switchPrePdiOrPDI(isPrePdi, order_tr_data){ 
					prePDIselected = isPrePdi; 
				//	console.log("prePDIselected "+prePDIselected);
					switchUI(3); 
//					pdiIssueAdderInit(order_tr_data);
		 }

