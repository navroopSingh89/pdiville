
//class IssueData
		function IssueData (id, issueType, roomlocation, description, _uniqueID) { 
				this.id = id;
				this.issueType = issueType;
				this.roomlocation = roomlocation;
				this.description = description;
                this.uniqueID = _uniqueID;
		}

//class issuesImage 
       function IssuesWithImage (title, description, imgsArray, _uniqueID,_trEditted) { 
                this.title = title;
                this.description = description;
                this.imgsArray = imgsArray;
                this.uniqueID = _uniqueID;
                this.trEditted = _trEditted;
        }
		
		 
//default variables	 
        var usrNameSpace = "pdigenUsr";
        var orderNameSpace ="pdigenOdr";
 
		var prePDIselected = null; 
		var PDIselected = null;

//used by issues cache		
		var loggerUser = null;
        var currentPage= null;


		var selectedOrderId = null; 
//		var issuesArrList = new Array();	
		
		
		//add backslash '/' after folder name
		var pageFolderName = "pages/"; 

//array to contain topic ids for deletion
    var IssueDeleteIds =[];

//system URL to access
var KVURL = null;

var KVIPToken ="";
//used by login page
        var kloudvilleDefaultLogo = "classic/resources/images/kloud_logo.png"; 
	
		  
       (function () { 
				//global variables for services 
                var loginService = 'service/idm/authentication/login';
                var userInfoService = 'service/idm/user/getUserInfo';
                var logoutService = 'service/idm/authentication/logout';
                var forgotPasswordService = 'service/idm/authentication/forgotPassword';
				var changePasswordService = 'service/idm/authentication/changePassword';
                 
                var createCustomerService = 'service/customer/manage/createCustomer';
				var exportCustomerService = 'service/customer/manage/getCustomer';
				var searchOrderService = 'service/order/manage/searchOrder';
				var multiPageSearchService = 'service/search/search/multiPageSearch';
				var getOrderService = 'service/order/manage/getOrder';
				var createOrderService = 'service/order/manage/createOrder';
				var updateOrderCartService = 'service/order/manage/priceCart';
				var updateOrderService = 'service/order/manage/updateOrder';
           
				var createNoteTopicService = 'service/customer/topic/createTopic';
                var getNoteByID = 'service/customer/topic/getTopic';
				var updateNotePostService = 'service/customer/topic/updatePost';
				var createNotePostService = 'service/customer/topic/addPostToTopic';
				var uploadAttachmentService = 'upload?$service=system.import.createResourceUrl';
				var getAllNoteTopicService = 'service/search/search/multiPageSearch';
                var getNotePostService = 'service/customer/topic/getPostList';
                var deleteNotesService = 'service/customer/topic/archiveTopic';
           
           
                var loginRequestType = 'com.kloudville.idm.userLogin';
                var msgResponseType = 'com.kloudville.system.core.message';
                var msgListResponseType = 'com.kloudville.system.core.messageList';

                window.kV = { 
                    ajax: function (service, data) {
						
                        return $.ajax(this.url+ service, { 
                            context: this,
                            contentType: 'application/json', //we send JSON
                            accepts: 'application/json',
                            method: 'POST',
                            async: true,
                            dataType: 'json',
                            crossDomain: true,
                            xhrFields: {
                                withCredentials: true
                            },
                            processData: false,
                            cache:false,
                            data: JSON.stringify(data),
                                success: function(data, textStatus, request) { 
                                //  console.log("request.getResponseHeader('Date')");
                               //   console.log(request.getResponseHeader('date'));
                              //    console.log(request.getAllResponseHeaders());
                                }
                        }) 
                            .then(
                                //failure - server sends back a status 200 with a JSON response explaining the problem
                                function (jsonResponse) { 
                                    var response, deferred = $.Deferred();

                                    //check that we are getting back a message or list of messages, display the text
                                    if (jsonResponse.hasOwnProperty('message')
                                            && jsonResponse.message.hasOwnProperty('__metadata')
                                            && jsonResponse.message.__metadata.type === msgResponseType) {

                                        response = jsonResponse.message.text;

                                        deferred.reject(response);
                                    } else if (jsonResponse.hasOwnProperty('messageList')
                                            && jsonResponse.messageList.hasOwnProperty('__metadata')
                                            && jsonResponse.messageList.__metadata.type === msgListResponseType) {

                                        var i, len = jsonResponse.messageList.message.length;
                                        response = [];
                                        for (i = 0; i < len; i++) {
                                            response.push(jsonResponse.messageList.message[i].text);
                                        }

                                        deferred.reject(response);
                                    } else {
                                        response = JSON.stringify(jsonResponse, undefined, 2);
                                        deferred.resolve(response);
                                    }

                                    return deferred;

                                },
                                //success - server sends back status 200 with empty response, but JQuery breaks trying to
                                //parse the empty response as JSON and treats it as a "failure".
                                function (jqXHR, status) {  
                                    var deferred = $.Deferred();  
                                    if (jqXHR.status === 200 && status == 'parsererror') {
                                        deferred.resolve('Login successful');
                                    } else {
                                        deferred.reject('Login failure'); 
                                    }

                                    return deferred;

                                }); 
								
                    },
					 
                    login: function (userName, password, url) {
                        this.url = url ? url + '/' : '';
                        return this.ajax(
                                loginService,
                                {
                                    userLogin: {
                                        __metadata: {
                                            type: loginRequestType
                                        },
                                        user: userName,
                                        password: password
                                    }
                                }
                        );

                    },
                    logout: function () {
                        return this.ajax(
                                logoutService
                        ); 
                    },
                     
                    forgotPassword: function (userName, email, mobileNum, url) { 
                        this.url = url ? url + '/' : '';
                        return this.ajax(
                                forgotPasswordService,
                                {
                                    forgotPassword: {
                                        mobilePhone: mobileNum,
                                        email: email,
                                        user: userName,
                                        __metadata: {
                                            type: "com.kloudville.idm.forgotPassword",
                                            version: null,
                                            id: null,
                                            displayName: null
                                        }
                                    }
                                }
                        ); 
                    },
                    
					changePassword: function (oldPass,newPass,retype) {
						return this.ajax(
							changePasswordService, 
							{
							objectRequest: {
									newPassword  : newPass,
									newPassword2 : retype,
									oldPassword  : oldPass,
								__metadata: {
									type: "com.kloudville.idm.changePassword",
									version: null,
									id: null,
									displayName: null
								}
							}
						}); 
					},
					getUserInfo: function (url) {
                        this.url = url ? url + '/' : '';
                        return this.ajax(
                                userInfoService 
                        ); 
                    },
                    
					exportCustomers: function () {
                        return this.ajax(
                                exportCustomerService, 
								{
								objectRequest:{
											id:"0bf9285b-6f1b-49a9-9c0f-e216e36bab41"
										}
								} 
                        );

                    },
					GetOrderById: function (_orderId) {
                        return this.ajax(
                                getOrderService, 
								{
								objectRequest:{
											id: _orderId,
											type: null,
											__metadata: {
												type: "com.kloudville.order.orderRequest",
												version: null,
												id: _orderId,
												displayName: null
											}
										}
								} 
                        );

                    },
					getAllOrdersList: function (_query,_sortby, _indexBy) {
                        return this.ajax(
                                searchOrderService+'?_=' + new Date().getTime(), 
								{
									objectRequest:{   
										 query:  _query,
										 metadata: null,
										 maxItems: null,
										 sortby: _sortby,
										 excludeFiltersFromResult: null,
										 index: _indexBy,
										 fullFacet: null,
										 searchId: null,
										 path: null,
                                         to :600,
                                         from :0,
										 filter: [{name: "type",value:["ORDER"] ,prohibited:null,system:null}]  
										}
								} 
                        ); 
                    },
					getAllOrdersListNoQuery: function (_sortby, _indexBy) {
                        return this.ajax(
                                searchOrderService+'?_=' + new Date().getTime(), 
								{
									objectRequest:{   
										 metadata: null,
										 maxItems: null,
										 sortby: _sortby,
										 excludeFiltersFromResult: null,
										 index: _indexBy,
										 fullFacet: null,
										 searchId: null,
										 path: null,
                                         to :600,
                                         from :0,
										 filter: [{name: "type",value:["ORDER"] ,prohibited:null,system:null}]  
										}
								} 
                        ); 
                    },
					CreateOrderByTypePPDI: function (order_data, visualIssues, hiddenIsses) {
                        return this.ajax(
                                createOrderService, 
								{ 
										order:{ 
															"type": "ORDER",
															"orderState": "NEW",
															"salesRep": order_data.FullOrder.salesRep,
															"orderPriority": order_data.FullOrder.orderPriority, 
															orderType: "PPDI", 
															description: order_data.description, 
//															"srcOrderId": order_data.FullOrder.id, 
															product: [{
																	__metadata: {
																		type: "com.kloudville.order.model.product",
																		id: "3b48b625-8ddc-4343-be24-fea1a676046b",
																		access: {
																			productId: "READONLY"
																		}
																	},
																	productCode: "PPDIDAT",
																	productCategory: "156934a0-9dce-478c-8c59-3ad5d1a98421",
																	productCatalog: "tarion-catalogue",
																	description: "Pre PDI form",
																	regularPrice: 0,
																	itemTotal: 0.0,
																	unitPrice: 0.0,
																	recurrence: "ONETIME",
																	priceCode: "PRC000461",
																	taxExempt: false,
																	manualPrice: false,
																	quantity: 1,
																	currency: "CAD",
																	charge: [],
																	tax: [{
																			__metadata: {
																				type: "com.kloudville.price.model.pricingItemTax"
																			},
																			taxName: "ON_HST",
																			label: "HST (ON)",
																			taxValue: 13.0,
																			taxAmount: 0.0,
																			taxTotal: 0.0
																		}
																	],
																	productType: "SERVICE",
																	quantityDiscount: false,
																	fractionalQuantity: false,
																	shippable: false,
																	productId: "3b48b625-8ddc-4343-be24-fea1a676046b",
																	shipToAddressId: order_data.FullOrder.customerInfo.shipToAddress.addressId,
																	configuration: {
																		__metadata: {
																			type: "com.kloudville.catalog.businesscatalog.tarion-catalogue.tarion_156934a0-9dce-478c-8c59-3ad5d1a98421",
																			version: "26",
																			id: "PPDIDAT",
																			displayName: "Pre PDI form",
																			access: {
																				addedDate: "READONLY",
																				productType: "READONLY",
																				productCode: "READONLY"
																			}
																		},
																		catalogId: "tarion-catalogue",
																		categoryName: "tarion",
																		categoryId: "156934a0-9dce-478c-8c59-3ad5d1a98421",
																		shortDescription: "Pre PDI form",
																		publishDate: "2016-11-16T10:41:00-05:00",
																		orderableDate: "2016-11-16T10:41:00-05:00",
																		addedDate: "2016-11-16T10:41:00-05:00",
																		shippable: false,
																		taxExempt: false,
																		manualPrice: false,
																		allowFractionalQuantity: false,
																		productType: "SERVICE",
																		configOptions: [{
																				__metadata: {
																					type: "com.kloudville.catalog.model.configOptions"
																				},
																				name: "issue",
																				mandatory: false,
																				hidden: true,
																				hiddenInPortal: true,
																				readonly: false,
																				readonlyInPortal: false,
																				showInSpec: true,
																				showInCart: true
																			}, {
																				__metadata: {
																					type: "com.kloudville.catalog.model.configOptions"
																				},
																				name: "issueType",
																				mandatory: false,
																				hidden: true,
																				hiddenInPortal: true,
																				readonly: false,
																				readonlyInPortal: false,
																				showInSpec: true,
																				showInCart: true,
																				listDisplayType: "LIST"
																			}, {
																				__metadata: {
																					type: "com.kloudville.catalog.model.configOptions"
																				},
																				name: "roomlocation",
																				mandatory: false,
																				hidden: true,
																				hiddenInPortal: true,
																				readonly: false,
																				readonlyInPortal: false,
																				showInSpec: true,
																				showInCart: true
																			}, {
																				__metadata: {
																					type: "com.kloudville.catalog.model.configOptions"
																				},
																				name: "issuedescription",
																				mandatory: false,
																				hidden: true,
																				hiddenInPortal: true,
																				readonly: false,
																				readonlyInPortal: false,
																				showInSpec: true,
																				showInCart: true
																			}, {
																				__metadata: {
																					type: "com.kloudville.catalog.model.configOptions"
																				},
																				name: "approved",
																				mandatory: false,
																				hidden: true,
																				hiddenInPortal: true,
																				readonly: false,
																				readonlyInPortal: false,
																				showInSpec: true,
																				showInCart: true,
																				listDisplayType: "LIST"
																			}, {
																				__metadata: {
																					type: "com.kloudville.catalog.model.configOptions"
																				},
																				name: "tarion_progress",
																				mandatory: false,
																				hidden: true,
																				hiddenInPortal: true,
																				readonly: false,
																				readonlyInPortal: false,
																				showInSpec: true,
																				showInCart: true,
																				listDisplayType: "LIST"
																			}, {
																				__metadata: {
																					type: "com.kloudville.catalog.model.configOptions"
																				},
																				name: "duedate",
																				mandatory: false,
																				hidden: true,
																				hiddenInPortal: true,
																				readonly: false,
																				readonlyInPortal: false,
																				showInSpec: true,
																				showInCart: true
																			}, {
																				__metadata: {
																					type: "com.kloudville.catalog.model.configOptions"
																				},
																				name: "notes",
																				mandatory: false,
																				hidden: true,
																				hiddenInPortal: true,
																				readonly: false,
																				readonlyInPortal: false,
																				showInSpec: true,
																				showInCart: true
																			}, {
																				__metadata: {
																					type: "com.kloudville.catalog.model.configOptions"
																				},
																				name: "vendorBuilderReference",
																				mandatory: false,
																				hidden: false,
																				readonly: false,
																				showInSpec: true,
																				showInCart: true
																			}, {
																				__metadata: {
																					type: "com.kloudville.catalog.model.configOptions"
																				},
																				name: "lot",
																				mandatory: false,
																				hidden: false,
																				readonly: false,
																				showInSpec: true,
																				showInCart: true
																			}, {
																				__metadata: {
																					type: "com.kloudville.catalog.model.configOptions"
																				},
																				name: "plan",
																				mandatory: false,
																				hidden: false,
																				readonly: false,
																				showInSpec: true,
																				showInCart: true
																			}, {
																				__metadata: {
																					type: "com.kloudville.catalog.model.configOptions"
																				},
																				name: "municipality",
																				mandatory: false,
																				hidden: false,
																				readonly: false,
																				showInSpec: true,
																				showInCart: true
																			}, {
																				__metadata: {
																					type: "com.kloudville.catalog.model.configOptions"
																				},
																				name: "condominiumProjectName",
																				mandatory: false,
																				hidden: false,
																				readonly: false,
																				showInSpec: true,
																				showInCart: true
																			}, {
																				__metadata: {
																					type: "com.kloudville.catalog.model.configOptions"
																				},
																				name: "level",
																				mandatory: false,
																				hidden: false,
																				readonly: false,
																				showInSpec: true,
																				showInCart: true
																			}, {
																				__metadata: {
																					type: "com.kloudville.catalog.model.configOptions"
																				},
																				name: "unit",
																				mandatory: false,
																				hidden: false,
																				readonly: false,
																				showInSpec: true,
																				showInCart: true
																			}, {
																				__metadata: {
																					type: "com.kloudville.catalog.model.configOptions"
																				},
																				name: "homeCivicAddress",
																				mandatory: false,
																				hidden: false,
																				readonly: false,
																				showInSpec: true,
																				showInCart: true
																			}, {
																				__metadata: {
																					type: "com.kloudville.catalog.model.configOptions"
																				},
																				name: "vendorBuilderName",
																				mandatory: false,
																				hidden: false,
																				readonly: false,
																				showInSpec: true,
																				showInCart: true
																			}, {
																				__metadata: {
																					type: "com.kloudville.catalog.model.configOptions"
																				},
																				name: "dateOfPossession",
																				mandatory: false,
																				hidden: false,
																				readonly: false,
																				showInSpec: true,
																				showInCart: true
																			}, {
																				__metadata: {
																					type: "com.kloudville.catalog.model.configOptions"
																				},
																				name: "unitEnrollment",
																				mandatory: false,
																				hidden: false,
																				readonly: false,
																				showInSpec: true,
																				showInCart: true
																			}, {
																				__metadata: {
																					type: "com.kloudville.catalog.model.configOptions"
																				},
																				name: "visualIssues",
																				mandatory: false,
																				hidden: false,
																				readonly: false,
																				showInSpec: true,
																				showInCart: true
																			}, {
																				__metadata: {
																					type: "com.kloudville.catalog.model.configOptions"
																				},
																				name: "hiddenIssues",
																				mandatory: false,
																				hidden: false,
																				readonly: false,
																				showInSpec: true,
																				showInCart: true
																			}, {
																				__metadata: {
																					type: "com.kloudville.catalog.model.configOptions"
																				},
																				name: "pdiDate",
																				mandatory: false,
																				hidden: false,
																				readonly: false,
																				showInSpec: true,
																				showInCart: true
																			}, {
																				__metadata: {
																					type: "com.kloudville.catalog.model.configOptions"
																				},
																				name: "representativesName",
																				mandatory: false,
																				hidden: false,
																				readonly: false,
																				showInSpec: true,
																				showInCart: true
																			}, {
																				__metadata: {
																					type: "com.kloudville.catalog.model.configOptions"
																				},
																				name: "signature",
																				mandatory: false,
																				hidden: false,
																				readonly: false,
																				showInSpec: false,
																				showInCart: false
																			}
																		],
																		image: [],
																		document: [],
																		productTitle: [{
																				__metadata: {
																					type: "com.kloudville.system.core.localizedText",
																					id: "en-xx",
																					access: {
																						language: "READONLY"
																					}
																				},
																				text: "Pre PDI form",
																				language: "en-xx"
																			}
																		],
																		productInfo: [],
																		productTag: [],
																		productCode: "PPDIDAT",
																		dateOfPossession:  order_data.dateOfPossession,
																		vendorBuilderReference: order_data.vendorRef ,
																		lot: order_data.lotNo,
																		plan: order_data.PlanNo,
																		municipality: order_data.Municipality,
																		condominiumProjectName: order_data.condoProjectName,
																		level: order_data.condoLevel,
																		unit: order_data.condoUnit,
																		homeCivicAddress: order_data.addressofInspectedHouse,
																		vendorBuilderName: order_data.vendorName,
																		representativesName: order_data.representativeName ,
																		pdiDate: order_data.datecreated,
																		unitEnrollment: order_data.unitEnrollmentNum,
																		visualIssues: visualIssues,
																		hiddenIssues: hiddenIsses,
                                                                        signature: order_data.encSign
																	}
																}
															],
															charge: [],
															shipToAddress: [order_data.FullOrder.customerInfo.shipToAddress],
															customerInfo: order_data.FullOrder.customerInfo,
															deliveryDate: order_data.dateOfPossession,
															partyRoleInfo: [],
															__metadata: {
																type: "com.kloudville.order.model.order",
																displayName: "Order null",
																access: {
																	type: "READONLY"
																},
																version: null,
																id: null
															} 
												} 
								} 
                        );

                    },
					CreateOrderByTypePDI: function (order_data, visualIssues, hiddenIsses) {
                        return this.ajax(
                                createOrderService, 
								{ 
										order:{ 
															"type": "ORDER",
															"orderState": "NEW",
															"salesRep": order_data.FullOrder.salesRep,
															"orderPriority": order_data.FullOrder.orderPriority, 
															orderType: "PDI", 
															description: order_data.description, 
//															"srcOrderId": order_data.FullOrder.id, 
															product: [
															{
																					"__metadata": {
																						"type": "com.kloudville.order.model.product",
																						"id": "f4a7e3b8-66dc-48bc-a43d-1cd03e07e91f",
																						"access": {
																							"productId": "READONLY"
																						}
																					},
																					"productCode": "PDIDAT",
																					"productCategory": "156934a0-9dce-478c-8c59-3ad5d1a98421",
																					"productCatalog": "tarion-catalogue",
																					"description": "PDI form",
																					"regularPrice": 0,
																					"itemTotal": 0.0,
																					"unitPrice": 0.0,
																					"recurrence": "ONETIME",
																					"priceCode": "PRC000461",
																					"taxExempt": false,
																					"manualPrice": false,
																					"quantity": 1,
																					"currency": "CAD",
																					"charge": [],
																					"tax": [{
																							"__metadata": {
																								"type": "com.kloudville.price.model.pricingItemTax"
																							},
																							"taxName": "ON_HST",
																							"label": "HST (ON)",
																							"taxValue": 13.0,
																							"taxAmount": 0.0,
																							"taxTotal": 0.0
																						}
																					],
																					"productType": "SERVICE",
																					"quantityDiscount": false,
																					"fractionalQuantity": false,
																					"shippable": false,
																					"productId": "f4a7e3b8-66dc-48bc-a43d-1cd03e07e91f",
																					"shipToAddressId": order_data.FullOrder.customerInfo.shipToAddress.addressId,
																					"configuration": {
																						"__metadata": {
																							"type": "com.kloudville.catalog.businesscatalog.tarion-catalogue.tarion_156934a0-9dce-478c-8c59-3ad5d1a98421",
																							"version": "26",
																							"id": "PDIDAT",
																							"displayName": "PDI form",
																							"access": {
																								"addedDate": "READONLY",
																								"productType": "READONLY",
																								"productCode": "READONLY"
																							}
																						},
																						"catalogId": "tarion-catalogue",
																						"categoryName": "tarion",
																						"categoryId": "156934a0-9dce-478c-8c59-3ad5d1a98421",
																						"shortDescription": "PDI form",
																						"publishDate": "2016-11-16T10:53:00-05:00",
																						"orderableDate": "2016-11-16T10:53:00-05:00",
																						"addedDate": "2016-11-16T10:53:00-05:00",
																						"shippable": false,
																						"taxExempt": false,
																						"manualPrice": false,
																						"allowFractionalQuantity": false,
																						"productType": "SERVICE",
																						"configOptions": [{
																								"__metadata": {
																									"type": "com.kloudville.catalog.model.configOptions"
																								},
																								"name": "issue",
																								"mandatory": false,
																								"hidden": true,
																								"hiddenInPortal": true,
																								"readonly": false,
																								"readonlyInPortal": false,
																								"showInSpec": true,
																								"showInCart": true
																							}, {
																								"__metadata": {
																									"type": "com.kloudville.catalog.model.configOptions"
																								},
																								"name": "issueType",
																								"mandatory": false,
																								"hidden": true,
																								"hiddenInPortal": true,
																								"readonly": false,
																								"readonlyInPortal": false,
																								"showInSpec": true,
																								"showInCart": true,
																								"listDisplayType": "LIST"
																							}, {
																								"__metadata": {
																									"type": "com.kloudville.catalog.model.configOptions"
																								},
																								"name": "roomlocation",
																								"mandatory": false,
																								"hidden": true,
																								"hiddenInPortal": true,
																								"readonly": false,
																								"readonlyInPortal": false,
																								"showInSpec": true,
																								"showInCart": true
																							}, {
																								"__metadata": {
																									"type": "com.kloudville.catalog.model.configOptions"
																								},
																								"name": "issuedescription",
																								"mandatory": false,
																								"hidden": true,
																								"hiddenInPortal": true,
																								"readonly": false,
																								"readonlyInPortal": false,
																								"showInSpec": true,
																								"showInCart": true
																							}, {
																								"__metadata": {
																									"type": "com.kloudville.catalog.model.configOptions"
																								},
																								"name": "approved",
																								"mandatory": false,
																								"hidden": true,
																								"hiddenInPortal": true,
																								"readonly": false,
																								"readonlyInPortal": false,
																								"showInSpec": true,
																								"showInCart": true,
																								"listDisplayType": "LIST"
																							}, {
																								"__metadata": {
																									"type": "com.kloudville.catalog.model.configOptions"
																								},
																								"name": "tarion_progress",
																								"mandatory": false,
																								"hidden": true,
																								"hiddenInPortal": true,
																								"readonly": false,
																								"readonlyInPortal": false,
																								"showInSpec": true,
																								"showInCart": true,
																								"listDisplayType": "LIST"
																							}, {
																								"__metadata": {
																									"type": "com.kloudville.catalog.model.configOptions"
																								},
																								"name": "duedate",
																								"mandatory": false,
																								"hidden": true,
																								"hiddenInPortal": true,
																								"readonly": false,
																								"readonlyInPortal": false,
																								"showInSpec": true,
																								"showInCart": true
																							}, {
																								"__metadata": {
																									"type": "com.kloudville.catalog.model.configOptions"
																								},
																								"name": "notes",
																								"mandatory": false,
																								"hidden": true,
																								"hiddenInPortal": true,
																								"readonly": false,
																								"readonlyInPortal": false,
																								"showInSpec": true,
																								"showInCart": true
																							}, {
																								"__metadata": {
																									"type": "com.kloudville.catalog.model.configOptions"
																								},
																								"name": "vendorBuilderReference",
																								"mandatory": false,
																								"hidden": false,
																								"readonly": false,
																								"showInSpec": true,
																								"showInCart": true
																							}, {
																								"__metadata": {
																									"type": "com.kloudville.catalog.model.configOptions"
																								},
																								"name": "lot",
																								"mandatory": false,
																								"hidden": false,
																								"readonly": false,
																								"showInSpec": true,
																								"showInCart": true
																							}, {
																								"__metadata": {
																									"type": "com.kloudville.catalog.model.configOptions"
																								},
																								"name": "plan",
																								"mandatory": false,
																								"hidden": false,
																								"readonly": false,
																								"showInSpec": true,
																								"showInCart": true
																							}, {
																								"__metadata": {
																									"type": "com.kloudville.catalog.model.configOptions"
																								},
																								"name": "municipality",
																								"mandatory": false,
																								"hidden": false,
																								"readonly": false,
																								"showInSpec": true,
																								"showInCart": true
																							}, {
																								"__metadata": {
																									"type": "com.kloudville.catalog.model.configOptions"
																								},
																								"name": "condominiumProjectName",
																								"mandatory": false,
																								"hidden": false,
																								"readonly": false,
																								"showInSpec": true,
																								"showInCart": true
																							}, {
																								"__metadata": {
																									"type": "com.kloudville.catalog.model.configOptions"
																								},
																								"name": "level",
																								"mandatory": false,
																								"hidden": false,
																								"readonly": false,
																								"showInSpec": true,
																								"showInCart": true
																							}, {
																								"__metadata": {
																									"type": "com.kloudville.catalog.model.configOptions"
																								},
																								"name": "unit",
																								"mandatory": false,
																								"hidden": false,
																								"readonly": false,
																								"showInSpec": true,
																								"showInCart": true
																							}, {
																								"__metadata": {
																									"type": "com.kloudville.catalog.model.configOptions"
																								},
																								"name": "homeCivicAddress",
																								"mandatory": false,
																								"hidden": false,
																								"readonly": false,
																								"showInSpec": true,
																								"showInCart": true
																							}, {
																								"__metadata": {
																									"type": "com.kloudville.catalog.model.configOptions"
																								},
																								"name": "vendorBuilderName",
																								"mandatory": false,
																								"hidden": false,
																								"readonly": false,
																								"showInSpec": true,
																								"showInCart": true
																							}, {
																								"__metadata": {
																									"type": "com.kloudville.catalog.model.configOptions"
																								},
																								"name": "dateOfPossession",
																								"mandatory": false,
																								"hidden": false,
																								"readonly": false,
																								"showInSpec": true,
																								"showInCart": true
																							}, {
																								"__metadata": {
																									"type": "com.kloudville.catalog.model.configOptions"
																								},
																								"name": "unitEnrollment",
																								"mandatory": false,
																								"hidden": false,
																								"readonly": false,
																								"showInSpec": true,
																								"showInCart": true
																							}, {
																								"__metadata": {
																									"type": "com.kloudville.catalog.model.configOptions"
																								},
																								"name": "visualIssues",
																								"mandatory": false,
																								"hidden": false,
																								"readonly": false,
																								"showInSpec": true,
																								"showInCart": true
																							}, {
																								"__metadata": {
																									"type": "com.kloudville.catalog.model.configOptions"
																								},
																								"name": "hiddenIssues",
																								"mandatory": false,
																								"hidden": false,
																								"readonly": false,
																								"showInSpec": true,
																								"showInCart": true
																							}, {
																								"__metadata": {
																									"type": "com.kloudville.catalog.model.configOptions"
																								},
																								"name": "pdiDate",
																								"mandatory": false,
																								"hidden": false,
																								"readonly": false,
																								"showInSpec": true,
																								"showInCart": true
																							}, {
																								"__metadata": {
																									"type": "com.kloudville.catalog.model.configOptions"
																								},
																								"name": "representativesName",
																								"mandatory": false,
																								"hidden": false,
																								"readonly": false,
																								"showInSpec": true,
																								"showInCart": true
																							}, {
                                                                                                __metadata: {
                                                                                                    type: "com.kloudville.catalog.model.configOptions"
                                                                                                },
                                                                                                name: "signature",
                                                                                                mandatory: false,
                                                                                                hidden: false,
                                                                                                readonly: false,
                                                                                                showInSpec: false,
                                                                                                showInCart: false
                                                                                            }
																						],
																						"image": [],
																						"document": [],
																						"productTitle": [{
																								"__metadata": {
																									"type": "com.kloudville.system.core.localizedText",
																									"id": "en-xx",
																									"access": {
																										"language": "READONLY"
																									}
																								},
																								"text": "PDI form",
																								"language": "en-xx"
																							}
																						],
																						"productInfo": [],
																						"productTag": [],
																						"productCode": "PDIDAT",
																						"dateOfPossession": order_data.dateOfPossession,
																						vendorBuilderReference: order_data.vendorRef ,
																						lot: order_data.lotNo,
																						plan: order_data.PlanNo,
																						municipality: order_data.Municipality,
																						condominiumProjectName: order_data.condoProjectName,
																						level: order_data.condoLevel,
																						unit: order_data.condoUnit,
																						homeCivicAddress: order_data.addressofInspectedHouse,
																						vendorBuilderName: order_data.vendorName,
																						representativesName: order_data.representativeName ,
																						pdiDate: order_data.datecreated,
																						unitEnrollment: order_data.unitEnrollmentNum,
																						visualIssues: visualIssues,
																						hiddenIssues: hiddenIsses,
                                                                                        signature: order_data.encSign
																					}
																				}
															],
															charge: [],
															shipToAddress: [order_data.FullOrder.customerInfo.shipToAddress],
															customerInfo: order_data.FullOrder.customerInfo,
															deliveryDate: order_data.dateOfPossession,
															partyRoleInfo: [],
															__metadata: {
																type: "com.kloudville.order.model.order",
																displayName: "Order null",
																access: {
																	type: "READONLY"
																},
																version: null,
																id: null
															} 
												} 
								} 
                        ); 
                    },
                    
                    
					UpdateOrderByTypePPDI: function (order_data, visualIssues, hiddenIsses) {
                        return this.ajax(
                                updateOrderCartService, 
								{ 
                                      order:order_data 
                                       }

                        );

                    },
					UpdateOrderByTypePDI: function (order_data, visualIssues, hiddenIsses) {
                        return this.ajax(
                                updateOrderCartService, 
								{ 
                                      order:order_data 
								} 
                        ); 
                    },
					UpdateOrderStatusforPDI: function (wholeNewOrder) {
                        return this.ajax(
                                updateOrderService, 
								{
                                    "order": {
                                        "lastPricedDate": wholeNewOrder.lastPricedDate,
                                        "repriceOnOpen": wholeNewOrder.repriceOnOpen,
                                        "locked": wholeNewOrder.locked,
                                        "currency":  wholeNewOrder.currency,
                                        "$currency":  wholeNewOrder.$currency,
                                        "type":  wholeNewOrder.type,
                                        "orderId": wholeNewOrder.orderId,
                                        "orderType": wholeNewOrder.orderType,
                                        "$orderType": wholeNewOrder.$orderType,
                                        "orderStatus": wholeNewOrder.orderStatus,
                                        "$orderStatus":  wholeNewOrder.$orderStatus,
                                        "orderSource":  wholeNewOrder.orderSource,
                                        "$orderSource":  wholeNewOrder.orderSource,
                                        "createdDate": wholeNewOrder.createdDate ,
                                        "submittedDate": wholeNewOrder.submittedDate,
                                        "createdBy": wholeNewOrder.createdBy,
                                        "salesRep": wholeNewOrder.salesRep,
                                        "$salesRep": wholeNewOrder.$salesRep,
                                        "salesChannel": wholeNewOrder.salesChannel,
                                        "$salesChannel": wholeNewOrder.$salesChannel,
                                        "accountManager": wholeNewOrder.accountManager,
                                        "$accountManager": wholeNewOrder.$accountManager,
                                        "description": wholeNewOrder.description,
                                        "lastUpdated": wholeNewOrder.lastUpdated,
                                        "hasAllocation": wholeNewOrder.hasAllocation,
                                        "orderPriority":  wholeNewOrder.orderPriority,
                                        "subscriptionId": wholeNewOrder.subscriptionId,
                                        "srcOrderId":  wholeNewOrder.srcOrderId,
                                        "trgOrderId":  wholeNewOrder.trgOrderId,
                                        "hasTask":  wholeNewOrder.hasTask,
                                        "completedDate":  wholeNewOrder.completedDate,
                                        "customerRequestedDate":  wholeNewOrder.customerRequestedDate,
                                        "deliveryDate":  wholeNewOrder.deliveryDate,
                                        "dueDate":  wholeNewOrder.dueDate,
                                        "shipFromLocationName": wholeNewOrder.shipFromLocationName,
                                        "processId": wholeNewOrder.processId,
                                        "shipFromLocationId": wholeNewOrder.shipFromLocationId,
                                        "customerPO": wholeNewOrder.customerPO,
                                        "extOrderId": wholeNewOrder.extOrderId,
                                        "campaign": wholeNewOrder.campaign,
                                        "advertisement": wholeNewOrder.advertisement,
                                        "orderState": wholeNewOrder.orderState,
                                        "taskState": wholeNewOrder.taskState,
                                        "salesTeam": wholeNewOrder.salesTeam,
                                        "activeSalesTeam": wholeNewOrder.activeSalesTeam,
                                        "currentSalesTeam": wholeNewOrder.currentSalesTeam,
                                        "territory": wholeNewOrder.territory,
                                        "activeAccountManager": wholeNewOrder.activeAccountManager,
                                        "currentAccountManager": wholeNewOrder.currentAccountManager,
                                        "product": wholeNewOrder.product,
                                        "charge": wholeNewOrder.charge,
                                        "cartTotal": wholeNewOrder.cartTotal,
                                        "shipToAddress":  wholeNewOrder.shipToAddress,
                                        "customerInfo":  wholeNewOrder.customerInfo,
                                        "partyRoleInfo":  wholeNewOrder.partyRoleInfo,
                                        "orderProgress":  wholeNewOrder.orderProgress,
                                        "progressHistory":  wholeNewOrder.progressHistory,
                                        "billingInfo":  wholeNewOrder.billingInfo,
                                        "paymentInfo":  wholeNewOrder.paymentInfo,
                                        "shippingInfo":  wholeNewOrder.shippingInfo,
                                        "__metadata": wholeNewOrder.__metadata
                                    }
                                }

                        ); 
                    }, 				
					multiPageSearchRequest: function (_query,_sortby, _indexBy, _from, _to) {
                        return this.ajax(
                                multiPageSearchService, 
								{
									objectRequest:{   
											service: "order.manage.searchOrder",
											searchId: "MzI3NDYzNjgyNjUwNzIwMA==",
											max: null,
											from: _from,
											to: _to,
											input: {
												query: _query,
												metadata: null,
												maxItems: null,
												sortby: _sortby,
												excludeFiltersFromResult: null,
												index: _indexBy,
												fullFacet: null,
												searchId: null,
												filter: [{
														name: "type",
														value: ["ORDER"]
													}
												],
												__metadata: {
													type: "com.kloudville.search.indexSearchRequest",
													version: null,
													id: null,
													displayName: null
												}
												},
												__metadata: {
													type: "com.kloudville.search.multipageSearchRequest",
													version: null,
													id: null,
													displayName: null
												}
										}
								} 
                        );

                    },
                    
                    createNote: function(uniqueId, orderId, issue_room_type,issues_decription, _attachment){
                        return this.ajax(
                                createNoteTopicService,{ 
                                        createTopicRequest: {
                                            topic: {
                                                editableInPortal: false,
                                                visibleInPortal: false,
                                                archived: null,
                                                subject: issue_room_type,
                                                contextCategory: "ORDER",
                                                topicId: uniqueId,
                                                contextId: orderId,
                                                createdDate: null,
                                                updatedDate: null,
                                                contentType: "GENERAL",
                                                availableTo: null,
                                                createdBy: null,
                                                updatedBy: null,
                                                __metadata: {
                                                    type: "com.kloudville.topic.model.topic",
                                                    displayName: issue_room_type,
                                                    access: {
                                                        topicId: "HIDDEN",
                                                        contextCategory: "HIDDEN",
                                                        contextId: "HIDDEN",
                                                        editableInPortal: "HIDDEN",
                                                        createdDate: "HIDDEN",
                                                        createdBy: "HIDDEN",
                                                        updatedDate: "HIDDEN",
                                                        updatedBy: 'HIDDEN'
                                                    },
                                                    version: null
                                                }
                                            },
                                            post: { 
                                                visibleInPortal: null,
                                                topicId: null,
                                                createdDate: null,
                                                updatedDate: null,
                                                topicType: null,
                                                messageId: null,
                                                contactName: null,
                                                contactMedium: null,
                                                content: issues_decription,
                                                createdBy: null,
                                                attachmentList: {
                                                    attachment: _attachment,
                                                    __metadata: {
                                                        type: "com.kloudville.topic.model.attachmentList",
                                                        version: null,
                                                        id: null,
                                                        displayName: null
                                                    }
                                                },
                                                __metadata: {
                                                    type: "com.kloudville.topic.model.topicPost", 
                                                    access: {
                                                        postId: "HIDDEN",
                                                        topicId: "HIDDEN",
                                                        createdDate: "HIDDEN",
                                                        createdBy: "HIDDEN",
                                                        attachmentList: "HIDDEN"
                                                    },
                                                    version: null,
                                                    displayName: null
                                                }
                                            },
                                            __metadata: {
                                                type: "com.kloudville.topic.createTopicRequest",
                                                version: null,
                                                id: null,
                                                displayName: null
                                            }
                                        } 

                                    
                                } 
                        );  
                    },
                    createPostInNote: function (topic_id, content, attachment) {
                        return this.ajax(
                            createNotePostService, { 
                                "topicPost": {
                                    "postId": null,
                                    "visibleInPortal": null,
                                    "topicId": topic_id,
                                    "createdDate": null,
                                    "updatedDate": null,
                                    "topicType": null,
                                    "messageId": null,
                                    "contactName": null,
                                    "contactMedium": null,
                                    "content": content,
                                    "createdBy": null,
                                    "attachmentList": {
                                        "attachment": attachment,
                                        "__metadata": {
                                            "type": "com.kloudville.topic.model.attachmentList",
                                            "version": null,
                                            "id": null,
                                            "displayName": null
                                        }
                                    },
                                    "__metadata": {
                                        "type": "com.kloudville.topic.model.topicPost",
                                        "id": null,
                                        "access": {
                                            "postId": "HIDDEN",
                                            "topicId": "HIDDEN",
                                            "createdDate": "HIDDEN",
                                            "createdBy": "HIDDEN",
                                            "attachmentList": "HIDDEN"
                                        },
                                        "version": null,
                                        "displayName": null
                                    }
                                }
                            } 
                        );
                    },
                    updateNote: function(updated_attachment){
                        return this.ajax(
                                updateNotePostService,{  
                                         "topicPost": updated_attachment
                                } 
                        );  
                    },
                    deleteNotes: function (modifiedOrder){
                            return this.ajax(
                                deleteNotesService, modifiedOrder
                       );
                    },
                    
                    getNoteDetailsByID: function (noteId) {
                        return this.ajax(
                            getNoteByID, {
                                topicRequest: {
                                              topicId: noteId,
                                              __metadata: {
                                               type: "com.kloudville.topic.topicRequest",
                                               version: null,
                                               id: null,
                                               displayName: null
                                         }
                                    }
                               }
                        );
                    },
                    getAllNoteTopic: function (order_id) {
                        return this.ajax(
                            getAllNoteTopicService, {
                                multipageSearchRequest: {
                                    service: "customer.topic.searchTopic",
                                    searchId: "NjE2MDY1MzYzNTc2MjAw",
                                    max: null,
                                    from: 0,
                                    to: 20,
                                    input: {
                                        query: null,
                                        metadata: null,
                                        maxItems: null,
                                        sortby: "updatedDate-",
                                        excludeFiltersFromResult: null,
                                        index: "TOPIC",
                                        fullFacet: null,
                                        showArchivedTopics: false,
                                        searchId: null,
                                        contextCategory: "ORDER",
                                        contextId: order_id,
                                        path: null,
                                        filter: [],
                                        __metadata: {
                                            type: "com.kloudville.topic.topicSearchRequest",
                                            version: null,
                                            id: null,
                                            displayName: null
                                        }
                                    },
                                    __metadata: {
                                        type: "com.kloudville.search.multipageSearchRequest",
                                        version: null,
                                        id: null,
                                        displayName: null
                                    }
                                }
                            } 
                        );
                    },
                    getNoteAttachments: function (topic_id) {
                        return this.ajax(
                            getNotePostService, {
                                postListRequest: {
                                    from: null,
                                    maxRows: 1000,
                                    desc: null,
                                    topicId: topic_id,
                                    __metadata: {
                                        type: "com.kloudville.topic.postListRequest",
                                        version: null,
                                        id: null,
                                        displayName: null
                                    }
                                }
                            } 
                        ); 
                    },
                    uploadAttachment: function(_data){
//                        return  [this.url + uploadAttachmentService];
                            return  $.ajax({
                                url: this.url + uploadAttachmentService ,
                                data: _data,
                                processData: false,  // Important!
                                type: 'POST',
                                async: false,
                                cache: false,   
                                contentType: false,  
                                enctype: 'multipart/form-data', 

    //                            //Before 1.5.1 you had to do this:
                                beforeSend: function (x) {
                                    if (x && x.overrideMimeType) {
                                        x.overrideMimeType("multipart/form-data");
                                    }
                                },
                                // Now you should be able to do this:
                                mimeType: 'multipart/form-data',    //Property added in 1.5.1

                                success: function (data) { 
//                                     console.log("SUCCESS : ", data);
                                    return data;
                                },
                                error: function (e) { 
//                                    console.log("ERROR : ", e);  
                                    return e;
                                }
                            });
                        
//                        return this.ajax(
//                                uploadAttachmentService,{
//                                    _data
//                                } 
//                        );  
                    },
                    createCustomer: function (firstName, lastName, accountNumber) {
                        return this.ajax(
                                createCustomerService,
                                {
                                    customer: {
                                        __metadata: {
                                            type: "com.kloudville.customer.model.customer"
                                        },
                                        partyRoleType: "CUSTOMER",
                                        accountManager: "bizAdmin",
                                        validFrom: "2016-08-11T22:14:04.825-04:00",
                                        customerStatus: "ACTIVE",
                                        identifier: [{
                                            __metadata: {
                                                type: "com.kloudville.party.model.identifier"
                                            },
                                            identifier: accountNumber,
                                            identifierType: "CUSTOMERID"
                                        }
                                        ],
                                        party: {
                                            __metadata: {
                                                type: "com.kloudville.party.model.individual"
                                            },
                                            partyType: "INDIVIDUAL",
                                            gender: "MALE",
                                            maritalStatus: "NEVER_MARRIED",
                                            partyName: [{
                                                __metadata: {
                                                    type: "com.kloudville.party.model.individualName"
                                                },
                                                partyNameType: "INDIVIDUAL",
                                                firstName: firstName,
                                                lastName: lastName
                                            }
                                            ]
                                        },
                                        contactMedium: [{
                                            __metadata: {
                                                type: "com.kloudville.party.model.emailContactMedium"
                                            },
                                            contactMediumType: "EMAIL",
                                            validFrom: "2016-08-11T22:13:15.934-04:00",
                                            emailType: "OTHER",
                                            email: "otherEmail@fdsa.ca"
                                        }, {
                                            __metadata: {
                                                type: "com.kloudville.party.model.phoneContactMedium"
                                            },
                                            contactMediumType: "PHONE",
                                            validFrom: "2016-08-11T22:13:32.068-04:00",
                                            phoneType: "FAX",
                                            phoneNr: "12321321"
                                        }, {
                                            __metadata: {
                                                type: "com.kloudville.party.model.addressContactMedium"
                                            },
                                            contactMediumType: "ADDRESS",
                                            validFrom: "2016-08-11T22:13:38.070-04:00",
                                            addressType: "MAIN",
                                            line1: "line1",
                                            line2: "line2",
                                            locality: "thornhill",
                                            postalcode: "L3J9C0",
                                            stateOrProvince: "NS",
                                            address: {
                                                __metadata: {
                                                    type: "com.kloudville.location.model.genericAddress"
                                                },
                                                line1: "line1",
                                                line2: "line2",
                                                postalcode: "L3J9C0",
                                                locality: "thornhill",
                                                country: "CA",
                                                stateOrProvince: "NS"
                                            }
                                        }
                                        ]
                                    }
                                }
                        );
                    }
                };

           
             
           
            })();
			 
	
 				
 //init functions	 
       $(document).ready(function () {    
                //make sure the login script is always loaded 
                $.getScript("js/loginHelper.js", function(){});    
           //encryption
//                $.getScript("js/sjcl.min.js", function(){});    
                
//           lightbox options
                 lightbox.option({
                      'resizeDuration': 10,
                      'wrapAround': true
                    })
            
				//show login screen
				switchUI(0);   
       });
 

//reset all ui to hidden an then switch what needed to show
		function switchUI(UInum){   
                $(bodyContainer).hide();
                
				switch (UInum) {
				//login UI
					case 0:   
                                if (!window.checkKvLogoSrc) { 
                                    includeJS("js/loginHelper.js"); 
                                }    
//                        includeJS("js/loginHelper.js");  
						$(loginContainer).hide().load( pageFolderName + 'login.html'+'?v='+ Date.now() , function() {  
                            //kv env logo loader
                            checkKvLogoSrc(); 
                            loadLoginUIElements();
                        }).fadeIn('500'); 
                        currentPage= 0;  
                            showHideMainLoader(false);
						break;
				//select Order UI
					case 1: 
                        $(loginContainer).hide();
                            showHideMainLoader(true);
                        if (!window.loadNavBtns) { 
                                    includeJS("js/navHelper.js"+'?v='+ Date.now()); 
                        }      
                        $.getScript("js/orderHelper.js", function(){});  
                        //load nav for any page first and then add the page into mainContentContainer
						$(bodyContainer).hide().load( pageFolderName + 'nav.html'+'?v='+ Date.now() , function() { 
//                            load order list here
                           $(mainContentContainer).hide().load( pageFolderName + 'orderselection.html'+'?v='+ Date.now(), function() { 
                                //load order list here 
                                loadOrderUIElements();  
                           }).fadeIn('500'); 
                            pushMenu();  
                        }).fadeIn('500');
                         
                        currentPage= 1; 
						break;
				//select Select Issue type UI
					case 2:
                        currentPage= 2;  
                        $(loginContainer).hide();
                        $(bodyContainer).show(); 
                        $(IssueContainer).hide();
                         
                        
//                        $.getScript("js/pdiSwitchHelper.js", function(){});
                         if (!window.loadPDISelectorUI) { 
                                    includeJS("js/pdiSwitchHelper.js"+'?v='+ Date.now()); 
                             } 
                            //load order list here
                           $(mainContentContainer).hide().load( pageFolderName + 'pdiselection.html'+'?v='+ Date.now(), function() { 
                                //load order list here 
                                loadPDISelectorUI();
                           }).fadeIn('500');  
                         
						break;
				//AddIssues UI
					case 3:
                        $(bodyContainer).show();
                        $(mainContentContainer).hide(); 
                        showHideMainLoader(false);
                        
                        if (!window.loadPdiIssuesUI) { 
                                    includeJS("js/pdiIssuesHelper.js"+'?v='+ Date.now()); 
                        } 
                        //donot reset if the order is is same and issue table already loaded
                        if ($(IssueContainer).html().length <10 &&  currentPage == 2) { 
//                        $.getScript("js/pdiIssuesHelper.js", function(){});
                               $(IssueContainer).hide().load( pageFolderName + 'pdiIssues.html'+'?v='+ Date.now(), function() { 
                                    //load order list here 
                                    loadPdiIssuesUI();
                               }).fadeIn('500');  
                       } else{
                           $('#topNavBar > div > div.navbar-header.navbar-default > ul > li:nth-child(8)').addClass('hidden');
                           $('#topNavBar > div > div.navbar-header.navbar-default > ul > li:nth-child(6)').removeClass("hidden active disabled"); 
                           $('#topNavBar > div > div.navbar-header.navbar-default > ul > li:nth-child(7)').addClass('active disabled').removeClass("hidden");
                           if(!$('#generateOrderp_Pdi').hasClass( "hidden" )){
                                $('#generateOrderp_Pdi').addClass('hidden');  
                           }
                           if($('#previewPDITable').hasClass( "hidden" )){
                                $('#previewPDITable').removeClass('hidden');  
                           } 
                            if(!$('#updateOrderp_Pdi').hasClass( "hidden" )){
                                      $('#updateOrderp_Pdi').addClass('hidden');  
                            } 
                           if(EditingOrder){
                               
                             $('#topNavBar > div > div.navbar-header.navbar-default > ul > li:nth-child(6)')
                                 .addClass('disabled').removeClass("hidden");
                           }

                           $(IssueContainer).show();   
                       } 

                        currentPage= 3;   
						break;
				//Preview PDI UI
					case 4: 
                        showHideMainLoader(true);
                        currentPage= 4;  
                        $(bodyContainer).show();
                        $(IssueContainer).hide();
                        if (!window.loadPreviewUI) { 
                                    includeJS("js/pdiPreviewHelper.js"+'?v='+ Date.now()); 
                                    includeJS("js/signature_pad.js");  
//                                    $('body').append($('<script>')
//                                             .attr('type', 'text/javascript').attr('src', "js/signature_pad.js"));
                        }
                        $.getScript("js/async.min.js", function(){});    
//                          $.getScript("js/signature_pad.js", function(){}); 
                        
                           $(mainContentContainer).hide().load( pageFolderName + 'pdiPreview.html'+'?v='+ Date.now(), function() { 
                                //load order list here  
                               loadPreviewUI();
                                showHideMainLoader(false);
                           }).fadeIn('500'); 
                         
						break; 
				} 
			}
	 

//function to show main loading dots
	    function showHideMainLoader(show){
					if(show){
						if($(mainloader).hasClass( "hidden" )){
							$(mainloader).removeClass('hidden');  
						}
					}else{
						if(!$(mainloader).hasClass( "hidden" )){
							$(mainloader).addClass('hidden');  
						}
					} 
	}

//alert message global function
        function showHideAlertMessage(message, success, time){ 
        if(success){
             $('#resultAlert').addClass('alert-success') .removeClass('alert-danger');

        }else{
             $('#resultAlert').addClass('alert-danger').removeClass('alert-success'); 
        }
          $('#resultAlert').fadeIn(); 

          $("#resultAlert").fadeTo(2000, 500).slideUp(500, function(){
                            $("#resultAlert").slideUp(500);
          });    

          $("#resultAlert #alertContent").text(message); 
    }

//include JS file in page body for entire session
        function includeJS(jsFile) {
            $('body').append($('<script>').attr('type', 'text/javascript').attr('src', jsFile));
        }


 

		//refresh everything before and after login	
		 function refreshEverything(isBeforeLogin){  
			  //reset variables  
				 selectedOrderId = null;
				 prePDIselected = null;
				   
				//date picker init for pdi preview
				$.fn.datepicker.defaults.format = "yyyy/mm/dd";
				$('.datepicker').datepicker({
					format: 'yyyy/mm/dd',
					todayHighlight: true, 
					autoclose: true
				});

				  
				list = new Array();
				pageList = new Array();
				currentPage = 1;
				numPerPage = 10;
				numberOfPages = 1;  // calculates the total number of pages
		
				_sortByElement = 'createdDate-';
		 
			 	orderSortIncr = true;
				searchQuery = "";
				orderSortCustIncr = true;
			 
		 } 
 
 

			 
			 	 
				 
		
		 

		 
	
 
	
	