    var wrapper, clearButton, savePNGButton, saveSVGButton, canvas, signaturePad;
//use namespace orderNameSpace
    var basil_issues;

    var issues_options = {
          // Namespace. Namespace your Basil stored data
          // default: 'b45i1'
          namespace: orderNameSpace,

          // storages. Specify all Basil supported storages and priority order
          // storages: `['local', 'cookie', 'session', 'memory']` 
          // expireDays. Default number of days before cookies expiration
          // default: 365
          expireDays: 31 
        };


//Function gets the signature.js file
//Gets the Signature pad by ID and wraps the buttons and canvas in a wrapper
function openSignature()
{       
    var script = $.getScript("js/signature_pad.js", function(){});                  
   
    console.log("Signature has been opened!");
    $('#signature-pad').removeClass('hidden');                                     
    
    wrapper = document.getElementById("signature-pad"),                            
    clearButton = wrapper.querySelector("[data-action=clear]"),                     
    savePNGButton = wrapper.querySelector("[data-action=save-png]"),                
    saveSVGButton = wrapper.querySelector("[data-action=save-svg]"),                
    canvas = wrapper.querySelector("canvas"),
    signaturePad; 
    
    if (signaturePad){                                                              //If the signature Pad exists, display the success message
        console.log("SUCCESS!!!!");
    }
    
// Adjust canvas coordinate space taking into account pixel ratio,
// Make it look crisp on mobile devices.
// This also causes canvas to be cleared.
function resizeCanvas() {
    // When zoomed out to less than 100%, for some very strange reason,
    // some browsers report devicePixelRatio as less than 1
    // and only part of the canvas is cleared then.
    var ratio =  Math.max(window.devicePixelRatio || 1, 1);
    canvas.width = canvas.offsetWidth * ratio;
    canvas.height = canvas.offsetHeight * ratio;
    canvas.getContext("2d").scale(ratio, ratio);
    
    console.log("Resized!");
}

window.onresize = resizeCanvas;
resizeCanvas();

signaturePad = new SignaturePad(canvas);

    
    //If the user clicks outside the pad, the signature is saved into the thumbnail
    $(document).mouseup(function(e) {
    var $container = $("#signature-pad");

    // if the target of the click isn't the container nor a descendant of the container
    if (!$container.is(e.target) && $container.has(e.target).length === 0) {
        
        console.log("You clicked outside!");
        saveSignature();
    }
});
    

clearButton.addEventListener("click", function (event) {
    
    signaturePad.clear();
    console.log("cleared");
});

savePNGButton.addEventListener("click", function (event) {
    if (signaturePad.isEmpty()) {
        alert("Please provide signature first.");
        
    $('#signature-pad').removeClass('hidden');
    $('#pdiPreview').addClass('hidden');
        
    } else {
        window.open(signaturePad.toDataURL());
    }
});

saveSVGButton.addEventListener("click", function (event) {
    if (signaturePad.isEmpty()) {
        alert("Please provide signature first.");
        
             
    $('#signature-pad').removeClass('hidden');
    $('#pdiPreview').addClass('hidden');
    } else {
        window.open(signaturePad.toDataURL('image/svg+xml'));
    }
});
    
    $('#pdiPreview').addClass('hidden');   
}


function saveSignature() 
{
    var canvas = document.getElementById('mycanvas');
    
    console.log("Save signature was clicked!");
    
    var dataURL = canvas.toDataURL(); 
    
    console.log(dataURL);

    
    if(dataURL != null){
        
    $('#signature-pad').addClass('hidden');
    $('#pdiPreview').removeClass('hidden');
    $('#signatureheading').addClass('hidden');
        
     document.getElementById('canvasImg').src = dataURL; 
   
     var desc = document.getElementById('SignatureTd');
     desc.style.backgroundColor = "white";
     console.log("Signature saved into thumbnail!");
    }
}
 

    var issuesArrList = new Array();

//pdi issues init function
    function loadPdiIssuesUI(){
//        console.log("UI loaded");   
       $('#topNavBar > div > div.navbar-header.navbar-default > ul > li:nth-child(6)')
           .removeClass("hidden active disabled"); 
       $('#topNavBar > div > div.navbar-header.navbar-default > ul > li:nth-child(7)')
           .addClass('active disabled').removeClass("hidden");
 
    //clear tables of issues
	   $("#tblVisual tbody").empty(); 
       $("#tblHidden  tbody").empty();
    
    //editable dopdown init
		loadEditableDropDown(); 
     
    //loading whole order from cart house id
        var wholeorder_tr_data =    $('#cart_house_Id').data("wholeOrderStored"); 
        
        pdiIssueAdderInit(wholeorder_tr_data);
}

//init pdi issue table adder	
	function pdiIssueAdderInit(order_tr_data){ 
			 //init editable select
			if($("div.ui-select-wrap").length == 0) { 
			  //it doesn't exist
				$('select.roomSelector').editableSelect();
			} 
			
			HideShowPreviewFunctionality(true);  
             //EditingOrder coming from switch order
               if(EditingOrder){
                   //load data of order into UI
                   loadOrderForEditing(order_tr_data);
                   
                   //checking images from server and loading into tables
                    loadImagesFromKV(order_tr_data.id);
               }else{
                    //check local storage for data saved and load if available
                    loadLocalDataToTables(); 
               }
             
			 
			 //save orderid to save savePDITable button
			$("#savePDITable").data("orderId", order_tr_data.id  );
			 
			//calling popup for saving and editing	
			popupModal(order_tr_data.id);
		  
			
			//show preview and load data	
			$('#previewPDITable').unbind().click(function(){
                 switchUI(4);  
			});   
             	//on click refresh cache load
			$('#loadCachePDITable').unbind().click(function(){
                 loadLocalDataToTables(); 
			});   
             
             
		 }
    // loading order data into table form for editing
    function loadOrderForEditing(wholeorder_tr_data){

        try{ 
//            console.log("order data loaded");
//            console.log(wholeorder_tr_data);
            var products_ = wholeorder_tr_data.FullOrder.product;
            var productNotFound = true;
            for (var key in products_){
                if((products_[key].productCode).includes("PDI")){ 
                    productNotFound = false;
                     $('#topNavBar > div > div.navbar-header.navbar-default > ul > li:nth-child(6)')
                         .addClass('disabled').removeClass("hidden");
      
                    var _hidden = (products_[key].configuration.hiddenIssues).trim();
                    var _visual = (products_[key].configuration.visualIssues).trim();           
                     
//                    console.log("products found ");
//                    console.log(products_[key].configuration);
                     
//                    console.log("products wholeorder_tr_data ");
//                    console.log(wholeorder_tr_data); 
                    wholeorder_tr_data.Municipality = (products_[key].configuration).municipality;
                    wholeorder_tr_data.PlanNo = (products_[key].configuration).plan;
                    wholeorder_tr_data.addressofInspectedHouse = (products_[key].configuration).homeCivicAddress;
                    wholeorder_tr_data.condoLevel = (products_[key].configuration).level;
                    wholeorder_tr_data.condoProjectName = (products_[key].configuration).condominiumProjectName;
                    wholeorder_tr_data.condoUnit = (products_[key].configuration).unit;
                    wholeorder_tr_data.dateOfPossession = (products_[key].configuration).dateOfPossession;
                    wholeorder_tr_data.lotNo = (products_[key].configuration).lot;
                    wholeorder_tr_data.unitEnrollmentNum = (products_[key].configuration).unitEnrollment;
                    wholeorder_tr_data.vendorName = (products_[key].configuration).vendorBuilderName;
                    wholeorder_tr_data.vendorRef = (products_[key].configuration).vendorBuilderReference; 
                    
                    wholeorder_tr_data.encSign = (products_[key].configuration).signature; 
                    
                    
                    //update the cart wholeorderData 
//                    wholeorder_tr_data.
                    $('#cart_house_Id').data("wholeOrderStored", wholeorder_tr_data); 
                    
//                    console.log("_hidden.length " +_hidden.length);
//                    console.log("_visual.length " +_visual.length);
                    
                    if(_hidden.length > 0 || _visual.length > 0 ){
                        showHideAlertMessage("Issues loaded from server successfully",true, 400);  
//					
                         if($(".issuesTablesContainer").hasClass("hidden")){
                            $(".issuesTablesContainer").removeClass("hidden");
                        }

                        if($(".issuesTablesContainer").hasClass("hidden")){
                            $(".issuesTablesContainer").removeClass("hidden");
                        }

                         OrderToTables("Visual" , ($(_visual).find('table')).prevObject[0].innerHTML);
                         OrderToTables("Hidden" , ($(_hidden).find('table')).prevObject[0].innerHTML); 
                        //show preview buttons
                         HideShowPreviewFunctionality(true);
                    }else{
                      //  alert("Unable to parse issues from order.");
//                        showHideAlertMessage("Unable to parse issues from order, Visual Issues: "+_visual.length + " Hidden Issues : "+_hidden.length , false, 300);
                    }
                    
                } 
            }
            
            if(productNotFound){ 
                        showHideAlertMessage("unable to retrieve issues from empty order.", false, 300);
               }
        }catch(err){
            console.log("err loading editing data" + err);
        }
    }
  
  // function to get the notes that have attachments
  function loadImagesFromKV(orderId) { 
//      console.log("loading images for order : "+ orderId);
      
      try{
            basil = new window.Basil(UsSpaceoptions); 
            var isloggedIN =  basil.get('IsloggedUser');
                 if(isloggedIN){ 
						//var url =   basil.get("lgR");  
                        url =   basil.get("lgR");
                        url = url.replace(/\/?$/, '/');
                      
                 async.waterfall([
                         function(callback) {
                             
                          kV.getAllNoteTopic(orderId).done(function (result) {
                              var notes_ = JSON.parse(result); 
                              
                              //set current notes in cart object
                            var wholeorder_tr_data =    $('#cart_house_Id').data("wholeOrderStored"); 
                                wholeorder_tr_data.KVnotes = notes_;
                            $('#cart_house_Id').data("wholeOrderStored", wholeorder_tr_data); 
//                              console.log(notes_);
                              var noteIDs =null;  
                              if(notes_.multipageSearchResult.result){ 
                                      var notesArray = notes_.multipageSearchResult.result;
                                      var notesArrayWithID = []; 
                                      for(var i = 0; i < notesArray.length; i++ ){                     
                                           if(notesArray.length > 0){ 
                                              //Grab all the Ids
                                              noteIDs = notesArray[i].id; 
                                              //Push Ids into the array
                                              notesArrayWithID.push(noteIDs);                          
                                          }
                                      }
                                    //Send the array of ids to the next function
                                    callback(null,notesArrayWithID);                                 
                                }
                              
                               }).fail(function (result) {  
                                      console.log("error" + result);
                          });
                             
                      },function(noteArrayIDs, callback){
                          console.log("no of notes found "+ noteArrayIDs.length); 
                          for(var i=0; i < noteArrayIDs.length; i++){    
                              //Loop through the Array of ids
                              var notesID = noteArrayIDs[i];   
                              //Send the note Ids to the getNoteAttachment service
                              kV.getNoteAttachments(notesID).done(function (returnValue) {  
                                   // send post object to function
                                  //Send the object containing the Note's Ids and the Note's contents
//                                  console.log("returnValue");
//                                  console.log(returnValue);
                                    AddAttachmentToIssueTable(JSON.parse(returnValue));                     
                              
                              }).fail(function (returnValue) {
                                      console.log("error" + returnValue );
                              });
                          }
                         
                        }
                    ]);
                        
                 }               
    }catch(err){}      
  }         

 


//Loops through the Note Obj and gets all the image names and image urls
    function AddAttachmentToIssueTable(_noteObj){ 
//        console.log(_noteObj);  
         var postArr = _noteObj.postList.post;
 //last element is latest
        var lastElement = postArr[postArr.length - 1];
         
        var _attachment =  lastElement.attachmentList.attachment; 
       
        if(_attachment){  
            for(var i = 0; i < _attachment.length; i++ ){  
                searchAndAddImage("Visual", _attachment[i].name,
                                  url + _attachment[i].url); 
                searchAndAddImage("Hidden", _attachment[i].name,
                                  url + _attachment[i].url); 
            } 
        } 
    }

//Searchs the Table type and adds image to the table
    function searchAndAddImage(_tableType, imagename, imageURL ){
      
        $('#tbl'+_tableType+' > tbody > tr').each(function () { 
                     if ($(this).closest('tr').attr('id')) {
                       var tr_unique_Id = $(this)[0].dataset.uniq_id; 
                       var rowId = $(this).closest('tr').attr('id'); 
//                     console.log(tr_unique_Id);    
//                     console.log(rowId);   
                         if(imagename){
                            if(imagename.includes(tr_unique_Id)){  
                                
//                                    console.log(rowId);
                                    //If the description matches the note, add the picture
                                    var aHrefTag = $("<a />",{"href":imageURL , "data-lightbox":'imgInp_id_'+rowId});
                                    var $img = $("<img />",{"src":imageURL});

                                    var divImgWrap = $("<div>", {"class": "img-wrap col-xs-3"});
                                    var closeSpan = '<div class=\"removeAtcImg close\" onclick=\"delAttachment(this)\">&times;</div>'; 
//
                                    $(divImgWrap).append(closeSpan);
                                    $(aHrefTag).append($img);
                                    $(divImgWrap).append(aHrefTag); 
                                 
//                                 console.log("base64");
//                                 console.log(base64);
                                if(($('#'+ rowId).has("div.idImgContainer").length > 0)){  
                                       var rowID =  $('#' +rowId); 
                                       var imgContainerID = rowID[0].id;                    //Get the Image container ID 
                                       $('#imgContainer_' +imgContainerID+"").append($(divImgWrap)[0].outerHTML);  
                                }else{ 
                                     var   markup2 =     '<tr><td>&nbsp;</td><td  style="padding-top: 12px;" colspan=\"3\">'+
                                                                                        '<div class="idImgContainer" id="imgContainer_'+rowId +'" class=\"row\">'+  
                                                    $(divImgWrap)[0].outerHTML+                                      
                                                    '</div></td></tr>';  
                                      $('#' +rowId+"> td > table > tbody").append(markup2);     //Append the image into the table row based on the row Id 
                                } 
                            }
                     }
                }
         });
     }

 
       
//function to read from order and add to UI
    function OrderToTables(type , tablecontent){
     
                var html = $.parseHTML(tablecontent);               //parse the data as a table  

                var innercontent = html[3].innerHTML;               //Grab the tbody of the parsed table 
                var parseContent = $.parseHTML(innercontent);       //Parse the results of the innercontent

                 for (var key in parseContent){  
                     var tableTD = ($(parseContent[key]).find("td"));
                     var id__ = tableTD[0].innerHTML;
                     var room__ = tableTD[1].innerHTML;
                     var description__ = tableTD[2].innerHTML;
  
                     var uniqueTRID = null;
                     var currentTR =  $(parseContent[key])[0]; 
                     
//                     console.log(currentTR);
                     
                     //If the current Table row contains dataset
                     if(currentTR.dataset){   
                         //Check if the current Table row  dataset's uniqueID is undefined 
                            if(typeof currentTR.dataset.uniq_id != 'undefined'){      
                                //If it is defined save the id into a variable
                               uniqueTRID = currentTR.dataset.uniq_id;               
                            }else{
                                uniqueTRID =  "rId_" +(new Date()).getTime(); 
                            }      
                     }
                     
                     AddRoomToComboBox(room__);
                       
                    var rowCount =  $("#tbl"+type+" > tbody > tr").length;
                    var serialnum = rowCount ;  
                    serialnum = serialnum + 1;

                      //get generated markup from the function
                    var markup2  = generateRowModal(type, serialnum, room__, description__, uniqueTRID); // "room", "desc"           //Send the innerHTML content
                         $("#tbl"+type+" tbody:first").append(markup2 ); 
 
                 } 
}

      // Add room to the editable drop down if room name doesnot exists in the list              
    function AddRoomToComboBox(__room){ 
                      __room =__room.trim().capitalize(); 
//              console.log("checking if dropdown contains " + __room );
              
              // see if element(s) exists that matches by 
              var exists = $('#issueRoom option:contains('+__room+')').length; 
//                        console.log(" contains "+__room+" : " +exists);
                if (!exists){ 
//                        console.log(" contains "+__room+" : " +exists);
                        $("<option>").val(__room).text(__room).attr('data-is-input', 1).appendTo(  $('#issueRoom'));  
                        $('div.ui-select-wrap').remove();
                        $('select.roomSelector').editableSelect(); 
                }  
          }
 
	//hide or show 	preview and save cache based on the the criteria
	function HideShowPreviewFunctionality(HideIt){
//            console.log("triggered hideshow " + HideIt);
			if(HideIt){ 
				if( ( $("#tblVisual tbody").children().length != 0) ||  ($("#tblHidden tbody").children().length != 0) ){
						if($('.issuesTablesContainer').hasClass( "hidden" )){
							$('.issuesTablesContainer').removeClass('hidden');  
						} 
						if($('.previewNsavePdi').hasClass( "hidden" )){
							$('.previewNsavePdi').removeClass('hidden');  
						}
						if($('#previewPDITable').hasClass( "hidden" )){
							$('#previewPDITable').removeClass('hidden');  
						}  
				}else{
					if(!$('.issuesTablesContainer').hasClass( "hidden" )){
								$('.issuesTablesContainer').addClass('hidden');  
							} 
					if(!$('.previewNsavePdi').hasClass( "hidden" )){
								$('.previewNsavePdi').addClass('hidden');  
					}
					if(!$('#previewPDITable').hasClass( "hidden" )){
							$('#previewPDITable').addClass('hidden');  
					}  
				}  
				
			}else{
				if($('.issuesTablesContainer').hasClass( "hidden" )){
							$('.issuesTablesContainer').removeClass('hidden');  
				} 
				if($('.previewNsavePdi').hasClass( "hidden" )){
							$('.previewNsavePdi').removeClass('hidden');  
				}
                if($('#previewPDITable').hasClass( "hidden" )){
                    $('#previewPDITable').removeClass('hidden');  
                }  
			}
		}  
		  
	//loading cache to issues table
	function loadLocalDataToTables(){ 
			//local storage retirever for that specific order
              
//             console.log("loading from cache");
             var order_idSelected =  $('#cart_house_Id').data("wholeOrderStored").id; 
//             console.log(order_idSelected);
                    basil_issues = new window.Basil(issues_options);
                    var visualTableContent = null;
					var hiddenTableContent = "";
             
				    var IsVisualCache =  basil_issues.get("IsVisualCache");
             
//             console.log(IsVisualCache);
             
                    if(IsVisualCache){
						        visualTableContent =   basil_issues.get(loggerUser+"_"+order_idSelected+'_Visual'+'_'+prePDIselected); 
                    }
                    var IsHiddenCache =  basil_issues.get("IsHiddenCache");
                    if(IsHiddenCache){
						        hiddenTableContent = basil_issues.get(loggerUser+"_"+order_idSelected+'_Hidden'+'_'+prePDIselected); 
                    }
              
               
					
				if(visualTableContent){ 
					$("#tblVisual tbody").empty();
//					console.log("visualTableContent");
//					console.log(visualTableContent);
					$("#tblVisual").removeClass("hidden"); 
					$("#tblVisual tbody").html(visualTableContent);
				}
				if(hiddenTableContent){
//					console.log("hiddenTableContent");
//					console.log(hiddenTableContent);
					$("#tblHidden tbody").html("");
					$("#tblHidden tbody").append(hiddenTableContent);
					$("#tblHidden").removeClass("hidden"); 
				} 
                if(visualTableContent || hiddenTableContent){ 
                  if( ( $("#tblVisual tbody").children().length != 0) ||  ($("#tblHidden tbody").children().length != 0) ){
				
                        HideShowPreviewFunctionality(false);
                    }  
                    showHideAlertMessage("Issues retrieved from local storage Successfully",true, 200);
                } else{
                     
//                    showHideAlertMessage("No Draft Found for this order",false, 200);
                }
		 }
	
	//popup modal for save and edit	
		 function popupModal(order_id){
			 issuesArrList= new Array();
			 
			 //clear visual issue
                $("#deleteAllVisual").unbind().click(function () {
						$("#tblVisual tbody").empty();  
                });   
				
			 //clear hidden issue
                $("#deleteAllHidden").unbind().click(function () {
						$("#tblHidden tbody").empty(); 
                }); 
//clear cache all table
                $("#deleteAllCache").unbind().click(function () {
				//		$("#tblVisual tbody").empty();
				//		$("#tblHidden tbody").empty(); 
                    basil_issues = new window.Basil(issues_options);
                    //reset only user namespace
                    basil_issues.reset(issues_options);  
						alert("cache has been cleared!");
                }); 
				
//save pdi table to cache only if clicked	
                $("#savePDITable").unbind().click(function () { 
                    
                        basil_issues = new window.Basil(issues_options); 
                     
                    
					//clear cache forst then add new 
//					if(canUseLocalStorage){ 
						 //order id for saving tabledata based on id
						var orderID = $( "#savePDITable").data( "orderId" );
					//	console.log("orderID in save type ppp " + prePDIselected);
						
					//	console.log(orderID);
						if( $("#tblVisual tbody").children().length != 0){
                            
						  basil_issues.set("IsVisualCache",true);
						  basil_issues.set(loggerUser+"_"+order_id+'_Visual'+'_'+prePDIselected, $("#tblVisual tbody").html());
//								localStorage.setItem(loggerUser+"_"+order_id+'_Visual'+'_'+prePDIselected, $("#tblVisual tbody").html()); 
                              
						}   
						if($("#tblHidden tbody").children().length != 0){ 
						  basil_issues.set("IsHiddenCache",true);
						  basil_issues.set(loggerUser+"_"+orderID+'_Hidden'+'_'+prePDIselected, $("#tblHidden tbody").html());
//							localStorage.setItem(loggerUser+"_"+orderID+'_Hidden'+'_'+prePDIselected, $("#tblHidden tbody").html());  
                              
						}
                    if( $("#tblVisual tbody").children().length != 0  || $("#tblHidden tbody").children().length != 0){
                        showHideAlertMessage("Issues Saved ",true, 200);  
//                        alert("Saved");
                    }
//					}
                }); 
			 
			 	//popup modal options on show
			$('#pdiAddModal').on('show.bs.modal', function (event) {
				  var button = $(event.relatedTarget) // Button that triggered the modal 
				  var recipient = button.data('whatever');
				  // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
				  // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
				  var modal = $(this);  
                
				  //if receipent == add then clear description
				  if(recipient == 'add'){
					//reset values
					  modal.find('.modal-body textarea').val(""); 
						if(!$('#editIssueBtn').hasClass( "hidden" )){
							$('#editIssueBtn').addClass('hidden');  
						}
						if($('#addIssueBtn').hasClass( "hidden" )){
							$('#addIssueBtn').removeClass('hidden');  
						}  
                        //clear input attachments 
                        $('#imgInp').val(''); 
                        $(".tmp_imgContainer").html("");
				  }
                
				  else if(recipient == 'edit'){  
                      var btnTrID = $(button).parent().parent().parent().parent().parent().parent().attr('id');  
//				 
                      var customData_uniqID = $(button).parent().parent().parent().parent().parent().parent()[0].dataset.uniq_id;  
//				      console.log(customData_uniqID);
                        $('#imgInp').val(''); 
                      
					var __serial = $('#'+btnTrID).find('.__serial').html();
					//	console.log(__serial);  
					var __room = $('#'+btnTrID).find('.__room').html();
                      
                    //add room to combo box even if not loaded
                    AddRoomToComboBox(__room);
                      
					//typeable dropdown value has to been shown also
					$(".ui-select-wrap div.selected span").text(__room);
					$("#issueRoom").val(__room);  
					$(".ui-select-wrap").val(__room);  
//						console.log(__room);
						
					var __issueType = btnTrID.split("_",1); 
					$("#issueType").val(__issueType);  
					 //setting row unique id in the comboc for saving in edit
					$("#issueType").data("uniqueId", customData_uniqID );  
                      
					var __desc = $('#'+btnTrID).find('.__desc').html();
					
					$("#issueTXT").val(__desc);  
                      
                      //copying input tag to modal
//                      console.log("copying content"); 
//                      imgContainer_Visual_1
//                      console.log('#imgContainer_'+__issueType+'_'+__serial); 
                      var currentImageContainer = $('#imgContainer_'+__issueType+'_'+__serial).html();
//                      var text = currentImageContainer.replace('id=", ', '');
                      
//                      console.log(currentImageContainer);
                       var divTmpImgWrap = null;
                      if ($(".tmp_imgContainer")[0]){
                            // Do something if class exists 
                           $(".tmp_imgContainer").html("");
                        } else {
                            divTmpImgWrap = $("<div>", {"class": "tmp_imgContainer"});
                        }
                      
                    var divTmpImgWrap = $("<div>", {"class": "tmp_imgContainer"});
                       
                    $(divTmpImgWrap).html(currentImageContainer);
                      
                    $('#imgInp').after(divTmpImgWrap); 
             
					//to remove that id and add new
					$( "#issueTXT" ).data( "rowId", btnTrID );
                        
					if($('#editIssueBtn').hasClass( "hidden" )){
						$('#editIssueBtn').removeClass('hidden');  
					}
					if(!$('#addIssueBtn').hasClass( "hidden" )){
						$('#addIssueBtn').addClass('hidden');  
					} 
					
				  }
				});
				
		//save  button click
			$('#pdiAddModal .modal-footer #addIssueBtn').unbind().on('click', function(event) { 
				//show preview functionality on adding something
				HideShowPreviewFunctionality(false);
                  
					var $button = $(event.target); // The clicked button 
					var room =  $(".ui-select-wrap div.selected span").text(); 
//					var room = $('#issueRoom').val();
					var type = $('#issueType').val();
					var desc = $('#issueTXT').val();
					
					if(!$('#editIssueBtn').hasClass( "hidden" )){
						$('#editIssueBtn').addClass('hidden');  
					}
					if($('#addIssueBtn').hasClass( "hidden" )){
						$('#addIssueBtn').removeClass('hidden');  
					} 
					
					
					if(desc.trim() != ""){
						//serial number for list
                        var rowCount =  $("#tbl"+type+" > tbody > tr").length;
						var serialnum = rowCount ;  
                        serialnum = serialnum + 1;
//                        console.log("length  add "+ serialnum);
						var uniqueID = "rId_" +(new Date()).getTime();
						 
						var issue = new IssueData (type+'_'+serialnum, type,  room, desc, uniqueID);
						issuesArrList.push(issue);
                          
                        //get generated markup from the function
                        var markup2  = generateRowModal(type, serialnum, room, desc, uniqueID);
						   
                        
						$("#tbl"+type+" tbody:first").append(markup2 ); 
                        
                        readImageTagURL($(this), type, serialnum); 
                          
						if($("#tbl"+type).hasClass( "hidden" )){
							$("#tbl"+type).removeClass("hidden"); 
						}
						
						 $('#issueTXT').val("");
						 $('#issueTXT').focus(); 
                        
                        //clear input attachments
                        $('#imgInp').val(''); 
                        
					 //  $('#pdiAddModal').modal('hide'); 
					}
				});
			
//Edit functionality has to add  
			$('#pdiAddModal .modal-footer #editIssueBtn').unbind().on('click', function(event) {
				  var $button = $(event.target); // The clicked button 
//					var room = $('#issueRoom').val(); 
					var room =  $(".ui-select-wrap div.selected span").text(); 
                
					var type = $('#issueType').val();
					var desc = $('#issueTXT').val();
					 
					 //delete that issue from the list and the local storage
					
					if(desc.trim() != ""){
                        
						var rowId = $('#issueTXT').data("rowId");  
                        var unique_ID = $("#issueType").data("uniqueId");  
                        
                        var selectedIndex = $("#"+rowId)[0].rowIndex; 
                        
//                        console.log("selectedIndex " + selectedIndex);
//                        console.log("rowId "+rowId);
                        
						$('#'+rowId).remove();    
                        
					    var rowNum = rowId.split("_",2);  
						var serialnum = rowNum[1] ;   
//                        console.log("length  "+serialnum);
                         
						var issue = new IssueData (type+'_'+serialnum, type,  room, desc, unique_ID); 
						issuesArrList.push(issue);
						 
                        //get generated markup from the function
                        var markup  = "";  
                        var loadAttach =false;
                        
                        
//                        check if any files has been added to input 
//                        if yes
//                            run add generateModal
//                          else 
//                               if  check  there is any img container in modal then   copy img from that
//                               else  add just rows
                        if( $("#imgInp")[0].files.length > 0){
                            //add generate modal
                            markup  = generateRowModal(type, serialnum, room, desc, unique_ID, true); 
                            loadAttach = true;
                            //clear tmp container on files add
                        }else{
                            loadAttach = false;
                            if($(".tmp_imgContainer").html.length > 0){
//                                console.log("has already imgs"); 
                                markup  = generateRowModalwithCopyIMG(type, serialnum, room, desc, unique_ID, true);  
                            }else{
//                                console.log("has no imgs ");
                                markup  = generateRowModal(type, serialnum, room, desc, unique_ID); 
                            }
                        }
                        
                            
                        
//						$("#tbl"+type+" tbody:first ").append(markup );  
						$("#tbl"+type+" tbody:first ").append(markup ); 
                        
//                        markup.insertAfter($("#tbl"+type+" tbody:first ").find('tr').eq(selectedIndex));  
                        if(loadAttach){
                            readImageTagURL($(this), type, serialnum); 
                        }
                        
                        
						 $('#issueTXT').val("");
						 $('#issueTXT').focus();
					   $('#pdiAddModal').modal('hide'); 
					    
					}
				});		
				
		// Find and remove selected table rows
			$(".delete-row").unbind().click(function(){
					$(".issuesTablesContainer table tbody").find('input[name="record"]').each(function(){
						if($(this).is(":checked")){
//                            console.log($(this).parents("tr")); 
                            var selectedTRArr = $(this).parents("tr");
                            for(var i =0; i< selectedTRArr.length; i++){ 
                                if(selectedTRArr[i].dataset.uniq_id){
//                                    console.log("tr found");
//                                    console.log(selectedTRArr[i].dataset.uniq_id);  
                                    IssueDeleteIds.push(selectedTRArr[i].dataset.uniq_id);
                                }
                            }
                             
							$(this).parents("tr").remove(); 
						}
					});
			 }); 
             
                
		 }
		 
 
//row modal generator
function generateRowModal(___type, ___serialnum, ___room, ___desc, ___uniqueID, isEditted){
    var markup2 = "";
       if( typeof isEditted === 'undefined' || isEditted === null ){
            isEditted =false;
        }
    //add only if there is any file attached
    if( $("#imgInp")[0].files.length > 0){ 
        markup2 =     '<tr><td>&nbsp;</td><td  style="padding-top: 12px;" colspan=\"3\">'+
                                                '<div class="idImgContainer" id="imgContainer_'+___type+'_'+___serialnum+'" class=\"row\">'+  
                                                '</div></td></tr>';
    }
     
            var markup = '<tr id="'+___type+'_'+___serialnum+'" data-uniq_Id="'+___uniqueID+'" data-editted="'+isEditted+'">'+ 
                                '<td colspan="4">'+
                    '<table style="width: 100%;"><tbody><tr>'+
											 '<td  style="width: 20px;"><input type="checkbox" name="record"></td>'+
											 '<td class="__serial hidden" style=\"display:none\">'+
												___serialnum +
												'</td><td class="__room">'+ ___room	+
												'</td><td class="__desc">'+	___desc +
												'</td><td style="width: 2%;">'+ 
                                                '<span data-toggle="modal" data-target="#pdiAddModal"'+
                                                'data-whatever="edit"><i class="fa fa-pencil" aria-hidden="true"></i></span>'+
//												'<button type="button" class="btn btn-primary" '
//            'data-toggle="modal" data-target="#pdiAddModal" data-whatever="edit">Edit</button>'+
												'</td></tr>' +
                                                markup2  +
                    '</tbody></table>'+
                                                '</td>'+
                                                '</tr>';

    return markup;
}

//copy img from container and add to row
function generateRowModalwithCopyIMG(___type, ___serialnum, ___room, ___desc, ___uniqueID, isEditted){
    var markup2 = "";
       
    //add only if there is any file attached
    if( $(".tmp_imgContainer").html.length > 0){ 
        markup2 =     '<tr><td>&nbsp;</td><td  style="padding-top: 12px;" colspan=\"3\">'+
                                                '<div class="idImgContainer" id="imgContainer_'+___type+'_'+___serialnum+'" class=\"row\">'+  
             $(".tmp_imgContainer").html() +                                      
            '</div></td></tr>';
    }
     
            var markup = '<tr id="'+___type+'_'+___serialnum+'" data-uniq_Id="'+___uniqueID+'" data-editted="'+isEditted+'">'+ 
                                '<td colspan="4">'+
                    '<table style="width: 100%;"><tbody><tr>'+
											 '<td style="width: 20px;"><input type="checkbox" name="record"></td>'+
											 '<td class="__serial hidden" style=\"display:none\">'+
												___serialnum +
												'</td><td class="__room">'+ ___room	+
												'</td><td class="__desc">'+	___desc +
												'</td><td style="width: 2%;">'+ 
												 '<span data-toggle="modal" data-target="#pdiAddModal" '+
                'data-whatever="edit"><i class="fa fa-pencil" aria-hidden="true"></i></span>'+
//											'<button type="button" class="btn btn-primary" data-toggle="modal" data-target="#pdiAddModal" data-whatever="edit">Edit</button>'+
												'</td></tr>' +
                                                markup2  +
                    '</tbody></table>'+
                                                '</td>'+
                                                '</tr>';

    return markup;
}

		//loadEditable dropdown
			function loadEditableDropDown(){
					(function($) {

                $.fn.editableSelect = function(options) {
                    var defaults = {
                        warpClass: 'ui-select-wrap',
                        editable: true
                    };
                    var opts = $.extend(defaults,options);

                    $(this).each(function(){
                        var $select     = $(this);
                        if($select.attr('data-editable') && $select.attr('data-editable') == 'false') {
                            opts.editable = false;
                        }
                        var html = '<div class="' + opts.warpClass + '" tabindex="-1">'
                        + '<div class="selected"><span></span>'
                        + '<a class="ui-button"><i class="glyphicon glyphicon-menu-down"></i></a></div>'
                        + '<div class="dropdown-box">';
                        html += (opts.editable ?
                            '<div class="inputbox"><input type="text" placeholder="add a new room" style="width:80%;" /> '
                                 +'<button class="input-group-addon addBtnCombo" id="comboxTxtAddBtn" type="submit"> <i class="fa fa-plus-circle" aria-hidden="true"></i></button>'
                                 + '</div>' : '');

                        html += '<ul></ul></div></div>';

                        var $selectWrap = $(html).insertAfter(this);
                        var $options    = $selectWrap.find('ul');
                        var $downbox = $selectWrap.find(".dropdown-box");
                        var bEdit = false;
                        $select.hide();
                        $downbox.css({
                            'width' : $selectWrap.width() + 'px'
                        });
                        $downbox.css({
                            'min-width' : '100%'
                        });
                        var _opt_click = function() {
                            var val  = $(this).attr('data-val'), txt = $(this).text();
                            $selectWrap.find(".selected").attr("data-val",val).find('span').text(txt);
                            $selectWrap.find(".selected").find('i').removeClass('glyphicon-menu-up').addClass('glyphicon-menu-down');
                            $select.val(val);
                            $downbox.hide();
                            $(this).addClass('over selected').siblings().removeClass('over selected');
                        };
                        var _new_opt = function(txt, val, isSelected) {
                            var $opt = $("<li>").text(txt).attr("data-val",val);
                            if(isSelected) {
                                $selectWrap.find(".selected").attr("data-val",val).find('span').text(txt);
                                $opt.addClass('over').addClass('selected');
                            }
                            $opt.click(_opt_click);
                            $opt.mouseenter(function(){
                                $(this).addClass('over').siblings().removeClass('over');
                            });
                            return $opt;
                        };
                        var _show = function(bShow) {
                            if(bShow) {
                                $selectWrap.find('i').removeClass('glyphicon-menu-down').addClass('glyphicon-menu-up');
                                if(!$options.find(".selected").hasClass('over')) {
                                    $options.find(".selected").addClass('over').siblings().removeClass('over');
                                }
                                $downbox.show();
                            } else {
                                $selectWrap.find("i").removeClass('glyphicon-menu-u').addClass('glyphicon-menu-down');
                                $downbox.hide();
                            }
                        };
                        $select.find('option').each(function() {
                            var $opt = _new_opt($(this).text(), $(this).val(), $(this).is(":selected"));
                            $opt.appendTo($options);
                        });
                        $selectWrap.find(".selected").click(function() {
                            _show($(this).find('i').hasClass('glyphicon-menu-down'));
                        });
                        $selectWrap.blur(function(event) {
                             event.preventDefault();
                             setTimeout(function() { if(bEdit) return; _show(false);}, 100);
                             $selectWrap.removeClass('focus');
                        });
                        if(opts.editable) {
                            var $input = $downbox.find("input");
                            $downbox.find(".inputbox").css('width', $selectWrap.width() - 22 + 'px');
                            $downbox.find(".inputbox").css({
                                'min-width' : '95%'
                            });
                            $input.focus(function(){
                                  $selectWrap.addClass('focus');
                                  bEdit = true;
                            });
                            $input.blur(function() {
                                bEdit = false;
                                $selectWrap.trigger('blur');
                            });
                            $input.bind('keypress',function(event){
                                if(event.keyCode == "13"){
                                    var val = $input.val();
                                    var $opt = _new_opt(val, val, true);
                                    $opt.appendTo($options);
                                    $input.val('');
                                    $("<option>").val(val).text(val).attr('data-is-input', 1).appendTo($select);
                                    $opt.trigger('click');
                                }
                            });
                            var $btnAdd = $downbox.find("#comboxTxtAddBtn");  
                        }
                        
                            $(document).on("click", "#comboxTxtAddBtn", function(){ 
                                    var val = $input.val();
                                    var $opt = _new_opt(val, val, true);
                                    $opt.appendTo($options);
                                    $input.val(''); 
                                    $("<option>").val(val).text(val).attr('data-is-input', 1).appendTo($select); 
                                    $opt.trigger('click'); 
                            }); 
                    });
                }
            })(jQuery);
				
}
			
//delete img from container
function delAttachment(param){   
      $(param).closest('.img-wrap').remove();
}


//read images from input and add them to img container of row
function readImageTagURL(objParent , issuetype, serial) {   
    var input  =   $("#imgInp")[0];
     
       var url;
       if (input.files  && input.files[0] ) {
            for (var i = 0; i < input.files.length; ++i){
                
                var reader = new FileReader();

                reader.onload = function (e) { 
                    var url  = e.target.result;
                    var aHrefTag = $("<a />",{"href":url , "data-lightbox":'imgInp_id_'+issuetype+'_'+serial});
                    var $img = $("<img />",{"src":url });

                    var divImgWrap = $("<div>", {"class": "img-wrap col-xs-3"});
                    var closeSpan = '<div class=\"removeAtcImg close\" onclick=\"delAttachment(this)\">&times;</div>'; 

                    $(divImgWrap).append(closeSpan);
                    $(aHrefTag).append($img);
                    $(divImgWrap).append(aHrefTag);
 
                    $('#imgContainer_'+issuetype+'_'+serial).append(divImgWrap); 
//                    console.log("added imgs");
                } 
                reader.readAsDataURL(input.files[i]); 
            }  
        }  
}




//to check if the upload is supported by the user
    function isUploadSupported() {
        if (navigator.userAgent.match(/(Android (1.0|1.1|1.5|1.6|2.0|2.1))|(Windows Phone (OS 7|8.0))|(XBLWP)|(ZuneWP)|(w(eb)?OSBrowser)|(webOS)|(Kindle\/(1.0|2.0|2.5|3.0))/)) {
            return false;
        }
        var elem = document.createElement('input');
        elem.type = 'file';
        return !elem.disabled;
    };
 
     function attachmentAdder(){
                 if (window.File && window.FileReader && window.FormData) {
                    var $inputField = $('#imgInp');

                    $inputField.on('change', function (e) {
                        var file = e.target.files[0];

                        if (file) {
                            if (/^image\//i.test(file.type)) {
                                readFile(file);
                            } else {
                                alert('Not a valid image!');
                            }
                        }
                    });
                } else {
                    alert("File upload is not supported!");
                } 
          
      }

        function readFile(file) {
            var reader = new FileReader();

            reader.onloadend = function () {
                console.log("file readed");
                processFile(reader.result, file.type);
            }

            reader.onerror = function () {
                alert('There was an error reading the file!');
            }

            reader.readAsDataURL(file);
        }

        function processFile(dataURL, fileType) {
            var maxWidth = 800;
            var maxHeight = 800;

            var image = new Image();
            image.src = dataURL;

            image.onload = function () {
                var width = image.width;
                var height = image.height;
                var shouldResize = (width > maxWidth) || (height > maxHeight);

                if (!shouldResize) {
//                    sendFile(dataURL);
                    return;
                }

                var newWidth;
                var newHeight;

                if (width > height) {
                    newHeight = height * (maxWidth / width);
                    newWidth = maxWidth;
                } else {
                    newWidth = width * (maxHeight / height);
                    newHeight = maxHeight;
                }

                var canvas = document.createElement('canvas');

                canvas.width = newWidth;
                canvas.height = newHeight;

                var context = canvas.getContext('2d');

                context.drawImage(this, 0, 0, newWidth, newHeight);

                dataURL = canvas.toDataURL(fileType);

                
                
            $('#blah').attr('src', e.target.result);
//                sendFile(dataURL);
            };

            image.onerror = function () {
                alert('There was an error processing your file!');
            };
        }
 
// customized function for Captilization
	 String.prototype.capitalize = function() {
            return this.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
        };