//**********************login page starts here
	 
        var pukey= "Secret Passphrase";
    var basil;

      var UsSpaceoptions = {
          // Namespace. Namespace your Basil stored data
          // default: 'b45i1'
          namespace: usrNameSpace,

          // storages. Specify all Basil supported storages and priority order
          // storages: `['local', 'cookie', 'session', 'memory']`
          
          // expireDays. Default number of days before cookies expiration
          // default: 365
          expireDays: 31 
        };

// check for url changes and load logo on first time
    function checkKvLogoSrc(){ 
        $("#envLogo").attr("src", "images/kloudvilleLogo.png"); 
        loadKVenvironmentLogo();
        $("#url").on('change keyup paste', function() { 
            loadKVenvironmentLogo();
        });
    }

//function to load kv selected environment logo based on url input
    function loadKVenvironmentLogo(){  
        try{
            var typedUrl =  $('input[name="url"]').val();
             if(typedUrl != undefined){
                 if ((typedUrl.trim() != "" || typedUrl.trim() != null ) && typedUrl.toLowerCase().includes("kloudville")){
                     typedUrl = typedUrl.replace(/\/?$/, '/'); 
                     
                     checkImage( typedUrl + kloudvilleDefaultLogo, function(){ 
                       $("#envLogo").attr("src", typedUrl + kloudvilleDefaultLogo); 
                     }, function(){ 
                     //if url doesnot exists
                            $("#envLogo").attr("src", "images/kloudvilleLogo.png"); 
                     } );
                     
                 }
             }
        }catch(err){
        }
     }

//check if image src is good or bad url
function checkImage(imageSrc, good, bad) {
    var img = new Image();
    img.onload = good; 
    img.onerror = bad;
    img.src = imageSrc;
}


 
//function to initialize the UI elements of the login
    function loadLoginUIElements(){ 
         $("#login").unbind().click(function () {
						LoginToKV(); 
         });   
        
        $("#password").unbind().keyup(function(ev) {
			// 13 is ENTER
			if (ev.which === 13 ) {
					LoginToKV(); 			 
			}
		}); 
         
        //auto login if cache exists
        checkUserCache();
    }
  
//login to KV environment
	function LoginToKV(){ 
					var user = $('input[name="user"]').val();
                    var pass = $('input[name="password"]').val();
                    var url = $('input[name="url"]').val(); 
                    //setting url to global access if needed
                     KVURL = url ;
					kvLoginNetworkCall(user, pass, url);
				}

//login network call for login
    function kvLoginNetworkCall(user, pass, url){
    
    showHideMainLoader(true);
             
					kV.login(user, pass, url).done(function (result) {
                        
                        kV.getUserInfo(url).done(function (userInfo) {  
						          //alert(userInfo);
						          setUserInfoCache(JSON.parse(userInfo), user, pass, url);
                             //only for savign issues based on current user 
                            	loggerUser = user.trim(); 
                            //	$("#username").html(loggerUser); 

                            //    showHideAlertMessage(result,true, 200);
                                //load UI only of login true
                                showHideMainLoader(false);

                                //load order ui
                                switchUI(1); 
                            
						}).fail(function (userInfo) {  
                                showHideAlertMessage('fail to get user info', false, 300);
						});
                        
                    }).fail(function (result) {  
                        showHideAlertMessage(result, false, 300);
						showHideMainLoader(false);
                    });
    
}
 
//function to call reset from kv environment
    function resetPassword(){
                var userId = $('input[name="userId"]').val();
                    var emailid = $('input[name="emailId"]').val();
                    var mobile = $('input[name="mobileId"]').val();
                    var url = $('input[name="url"]').val(); 
        
					showHideMainLoader(true);
                    if((userId.trim().length > 0 ) && (url.trim().length > 0))
                    { 
                        kV.forgotPassword(userId, emailId, mobile,url).done(function (result) {

                            showHideAlertMessage(result,true, 200);
                                    //load UI only of login true
                                    showHideMainLoader(false);
                                //	switchUI(1);   
                                }).fail(function (result) {  
                                    showHideAlertMessage(result, false, 300);
                                    showHideMainLoader(false);
                                });

                    } 
     }
   

//function to set user info in session cache
	 function setUserInfoCache(userInfo, user, pass, url){
                basil = new window.Basil(UsSpaceoptions); 
						basil.set("IsloggedUser",true);
						basil.set("lgU",Encrypt(user));
						basil.set("lgP",Encrypt(pass));
						basil.set("lgR",url);
             
						basil.set("firstName",userInfo.userInfo.firstName);
						basil.set("lastName",userInfo.userInfo.lastName);
						basil.set("userId",userInfo.userInfo.userId);
						basil.set("realm",userInfo.userInfo.realm);
						basil.set("status",userInfo.userInfo.status);
						basil.set("language",userInfo.userInfo.language);
						basil.set("currency",userInfo.userInfo.currency);	 
	}


//auto check user cache to load data
    function checkUserCache(){
        try{
            basil = new window.Basil(UsSpaceoptions); 
            var isloggedIN =  basil.get('IsloggedUser');
        //    console.log(isloggedIN); 
                 if(isloggedIN){ 
                        var user = basil.get("lgU");
						var pass =  basil.get("lgP");
						var url =   basil.get("lgR");
                       // console.log("user already logged in "+isloggedIN);
                      kvLoginNetworkCall(Encrypt(user), Decrypt(pass) , url);
                }  
                     
                     
        }catch(err){}   
    }

function Encrypt(str) {
    if (!str) str = "";
    str = (str == "undefined" || str == "null") ? "" : str;
    try {
        var key = 146;
        var pos = 0;
        ostr = '';
        while (pos < str.length) {
            ostr = ostr + String.fromCharCode(str.charCodeAt(pos) ^ key);
            pos += 1;
        }

        return ostr;
    } catch (ex) {
        return '';
    }
}
 
function Decrypt(str) {
    if (!str) str = "";
    str = (str == "undefined" || str == "null") ? "" : str;
    try {
        var key = 146;
        var pos = 0;
        ostr = '';
        while (pos < str.length) {
            ostr = ostr + String.fromCharCode(key ^ str.charCodeAt(pos));
            pos += 1;
        }

        return ostr;
    } catch (ex) {
        return '';
    }
}


//**********************login page starts here