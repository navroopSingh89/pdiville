 var wrapper, clearButton, saveButton, canvas, signaturePad;
    var canvas2 =null;
    var signaturePad2 = null;

        var defaultColor ="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAQAAAC1HAwCAAAAC0lEQVR42mNk+A8AAQUBAScY42YAAAAASUVORK5CYII=";

        var imgs_arr_forsingleIssue =[];

        var NotesList = [];
        var _wholeorder_tr_data = null;

      function loadPreviewUI(){
//    console.log("preview loaded");
    //reset notes to load again
            NotesList = [];

            if(!$('#previewPDITable').hasClass( "hidden" )){
                    $('#previewPDITable').addClass('hidden');  
            } 

            if(EditingOrder){
                   $('#topNavBar > div > div.navbar-header.navbar-default > ul > li:nth-child(6)').removeClass("hidden active disabled"); 
                   $('#topNavBar > div > div.navbar-header.navbar-default > ul > li:nth-child(7)').removeClass("hidden active disabled"); 
                   $('#topNavBar > div > div.navbar-header.navbar-default > ul > li:nth-child(8)').addClass('active').removeClass("hidden");
                        $('#topNavBar > div > div.navbar-header.navbar-default > ul > li:nth-child(6)')
                                 .addClass('disabled').removeClass("hidden");

                    if($('#updateOrderp_Pdi').hasClass( "hidden" )){
                            $('#updateOrderp_Pdi').removeClass('hidden');  
                    } 

            }else{ 
               $('#topNavBar > div > div.navbar-header.navbar-default > ul > li:nth-child(6)').removeClass("hidden active disabled"); 
               $('#topNavBar > div > div.navbar-header.navbar-default > ul > li:nth-child(7)').removeClass("hidden active disabled"); 
               $('#topNavBar > div > div.navbar-header.navbar-default > ul > li:nth-child(8)').addClass('active').removeClass("hidden");


                if($('#generateOrderp_Pdi').hasClass( "hidden" )){
                        $('#generateOrderp_Pdi').removeClass('hidden');  
                } 
            }


            //loading whole order from cart house id
              _wholeorder_tr_data =    $('#cart_house_Id').data("wholeOrderStored"); 

                loadEditableIds(); 
                loadSignatureUI(); 

        //    showHideAlertMessage("Click Generate Order when done",true, 200); 
                try{
                         //loading s rom the order into the preview
                    loadDatafromIssuestoPDI(_wholeorder_tr_data);
                }catch(err){ 
        //                    console.log("err "+ err);
                        }
                try{
                            //after loadng data init generate button
                    InitGeneratePDI(prePDIselected, _wholeorder_tr_data);
                }catch(err){ 
        //            console.log("err "+ err);
                }

            try{
                            //after loadng data init generate button
                    InitUpdatePDI(prePDIselected, _wholeorder_tr_data);
                }catch(err){ 
        //            console.log("err "+ err);
                }
     
}

 //making fields ceditables
      function loadEditableIds(){
        //for xeditable
//  //        var defaults = {
//            mode: 'inline'     
//        };
//$.extend($.fn.editable.defaults, defaults);
//  
    
    $.fn.datepicker.defaults.format = "yyyy/mm/dd";
    
		$('.datepicker').datepicker({
					format: 'yyyy/mm/dd',
					todayHighlight: true, 
					autoclose: true
				});
    
    $('#dateOfPossession').on('changeDate', function(ev){
        $(this).datepicker('hide');
    });
    $('#signedDate').on('changeDate', function(ev){
        $(this).datepicker('hide');
    });
    
     $('#vendorBuilderReference').editable();
    
//     $('#UnitEnrol1').editable();
     $('#lotNum').editable();
     $('#planNum').editable();
     $('#MuniciplaityName').editable();
     $('#CondoProjectName').editable();
     $('#levelNum').editable();
     $('#UnitNum').editable();
     $('#homeAddressline1').editable();
     $('#vendorBuilderName').editable(); 
      
}
		  
//local data storage functions ends 
	 function loadDatafromIssuestoPDI(order_data){
			//	console.log(order_data);
			//clear both tables to make sure
					 $('#tblVisualPreview tbody').empty();
					 $('#tblHiddenPreview tbody').empty();
					 
             var V_indexNum = 1;
             var H_indexNum = 1;
					 
				//copy data from issue tables to preview	
				$('#tblVisual tr').each(function(i) {
						if (!this.rowIndex) return; // skip first row\\
                      
             //td contains data has id and without is showing from img container
                      if($(this).attr('id')){
                            
                      //table contains data plus img container
                       var insideTableTR =  $(this.cells[0]).find('table');
                        
                       var  $tr = $(insideTableTR[0]); 
                                  
                        var currentTR = $(this)[0];                             //Grab the current Table row
                        var uniqueTRID = null;  
//                        console.log(currentTR.dataset.uniq_id);  
                        if(currentTR.dataset){                                          //If the current Table row contains dataset
                                if(typeof currentTR.dataset.uniq_id != 'undefined'){         //Check if the current Table row  dataset's uniqueID is undefined 

                                 //  console.log("The data set is defined");
                                //   console.log(currentTR.dataset.uniq_id);        

                                   uniqueTRID = currentTR.dataset.uniq_id;                //If it is defined save the id into a variable
                            }         
                         }
                          
//                       console.log($tr); 
//                       console.log((($tr.find(".__room"))[0]).innerHTML);
                           
                      var issueRoom = (($tr.find(".__room"))[0]).innerHTML;
                      var issueDescr = (($tr.find(".__desc"))[0]).innerHTML;
                        
                          try{
                              if(($tr.find(".idImgContainer"))[0]){
                                  var imagesContainer = (($tr.find(".idImgContainer"))[0]).innerHTML; 
                                  var imgTags = ($(imagesContainer).find("img"));
                                  var imgsArray = [];

                                  var title_note = "Visual - "+issueRoom;  
//                                console.log(title_note);     
                                  
                                  for(var j=0; j<imgTags.length ;j++){ 
//                                           console.log("imgTags" +j); 
//                                           console.log($(imgTags[j]).attr("src"));
//                                           console.log(imgTags[j].attr(src));
                                        imgsArray.push($(imgTags[j]).attr("src"));
                                  }

//                                  console.log("imgsArray");  
//                                  console.log(imgsArray.length);   
                                  
                                  
    //                              Visual Issues -  Bedroom
                                    var singleNoteWithAttachments = new  IssuesWithImage (title_note, issueDescr, imgsArray, uniqueTRID); 
                             
                                    NotesList.push(singleNoteWithAttachments);

                              }
                          }catch(err){
                              console.log("err "+err);
                          }
						 
						var markup2 = "<tr  data-uniq_Id='"+uniqueTRID+"'><td class=\"paddedClm\"  style=\" border-left: solid 1px #000000; border-bottom: solid 1px #000000;\">"+ V_indexNum++ + 
										"</td><td class=\"paddedClm\"  contenteditable=\"true\" style=\" border-left: solid 1px #000000; border-bottom: solid 1px #000000;\">"+ issueRoom + 
										"</td><td class=\"paddedClm\"  contenteditable=\"true\"  style=\" border-left: solid 1px #000000; border-bottom: solid 1px #000000;\">" +
										issueDescr + 
										"</td></tr>";
										  
                          $("#tblVisualPreview tbody").append(	markup2 );  
                      } 
					});
		  
                $('#tblHidden tr').each(function(i) {
						if (!this.rowIndex) return; // skip first row\\
                      
             //td contains data has id and without is showing from img container
                      if($(this).attr('id')){
                            
                      //table contains data plus img container
                       var insideTableTR =  $(this.cells[0]).find('table');
                        
                       var  $tr = $(insideTableTR[0]);   
                          
                        var currentTR = $(this)[0];                             //Grab the current Table row
                        var uniqueTRID = null;  
//                        console.log(currentTR.dataset.uniq_id);  
                        if(currentTR.dataset){                                          //If the current Table row contains dataset
                                if(typeof currentTR.dataset.uniq_id != 'undefined'){         //Check if the current Table row  dataset's uniqueID is undefined 

                                 //  console.log("The data set is defined");
                                //   console.log(currentTR.dataset.uniq_id);        

                                   uniqueTRID = currentTR.dataset.uniq_id;                //If it is defined save the id into a variable
                            }         
                         }
                          
                          var TREditted =false;
                          if(currentTR.dataset){                                          //If the current Table row contains dataset
                                if(typeof currentTR.dataset.editted != 'undefined'){         //Check if the current Table row  dataset's uniqueID is undefined 
                                        TREditted = currentTR.dataset.editted;                //If it is defined save the id into a variable
                            }         
                          }
                          
                          
                          
                      var issueRoom = (($tr.find(".__room"))[0]).innerHTML;
                      var issueDescr = (($tr.find(".__desc"))[0]).innerHTML;
  
                          try{
                              if(($tr.find(".idImgContainer"))[0]){
                                  var imagesContainer = (($tr.find(".idImgContainer"))[0]).innerHTML; 
                                  var imgTags = ($(imagesContainer).find("img"));
                                  var imgsArray = [];

                                  var title_note = "Hidden - "+issueRoom ;  
//                                console.log(title_note);     
//                                console.log("TREditted "+TREditted);     
                                  
                                  for(var j=0; j<imgTags.length ;j++){ 
//                                           console.log("imgTags" +j); 
//                                           console.log($(imgTags[j]).attr("src"));
//                                           console.log($(imgTags[j]).attr("src"));
                                        imgsArray.push($(imgTags[j]).attr("src"));
                                  }

//                                  console.log("imgsArray");  
//                                  console.log(imgsArray.length);   
                                   
    //                              Visual Issues -  Bedroom
                                    var singleNoteWithAttachments = new  IssuesWithImage (title_note, issueDescr, imgsArray, uniqueTRID, TREditted); 
                             
                                    NotesList.push(singleNoteWithAttachments);

                              }
                          }catch(err){
                              console.log("err "+err);
                          }
                          
						 
						var markup2 = "<tr data-uniq_Id='"+uniqueTRID+"'><td class=\"paddedClm\"  style=\" border-left: solid 1px #000000; border-bottom: solid 1px #000000;\">"+ H_indexNum++ + 
										"</td><td class=\"paddedClm\"  contenteditable=\"true\" style=\" border-left: solid 1px #000000; border-bottom: solid 1px #000000;\">"+ issueRoom + 
										"</td><td class=\"paddedClm\"  contenteditable=\"true\"  style=\" border-left: solid 1px #000000; border-bottom: solid 1px #000000;\">" +
										issueDescr + 
										"</td></tr>";
										  
                          $("#tblHiddenPreview tbody").append(	markup2 );  
                      } 
					});
             
             
				//initalize picker elemets 
				var datenow = new Date();
				if(order_data.dateOfPossession){
					datenow = new Date(order_data.dateOfPossession); 
				}
             
				$('#dateOfPossession').datepicker('update',datenow);
				
				$('#UnitEnrol1').html(order_data.unitEnrollmentNum); 
				$('#UnitEnrol2').html(order_data.unitEnrollmentNum); 
				
				$("#UnitEnrol1").on("change paste keyup", function() {  
				   if($('#UnitEnrol2').html() != $(this).html() )
				   {  $('#UnitEnrol2').html($(this).html()); }
				});
				$("#UnitEnrol2").on("change paste keyup", function() { 
				   if($('#UnitEnrol1').html() != $(this).html() ){
					   $('#UnitEnrol1').html($(this).html());
					   }
				});
				
			//$('#UnitEnrol2').html(order_data.unitEnrollmentNum); 
				 //vendor and address info 
				$('#vendorBuilderReference').html(order_data.vendorRef); 
				
				 $('#lotNum').html(order_data.lotNo);
				 $('#planNum').html(order_data.PlanNo);
				 $('#MuniciplaityName').html(order_data.Municipality);
				 
				 $('#CondoProjectName').html(order_data.condoProjectName);
				 $('#levelNum').html(order_data.condoLevel);
				 $('#UnitNum').html(order_data.condoUnit);
				 
				 $('#homeAddressline1').html(order_data.addressofInspectedHouse);
				 
				 $('#vendorBuilderName').html(order_data.vendorName);
				 
				 $('#representativesName').html(order_data.representativeName); 
				  
                 if(order_data.encSign){
                    signaturePad2.fromDataURL(order_data.encSign); 
                 }
				   
				//customer info init
				 $('#txtPurchaserName').html(order_data.customerName);
				 $('#txtPurchaserName2').html(order_data.purchaserName2);
				 $('#txtDesignateName').html(order_data.DesignateName);
				 $('#signedDate').datepicker('update', new Date());   
         
//            console.log("preview order_data");
//            console.log(order_data);
 
		 }
 
 
 //generate pdi
		 function InitGeneratePDI(isPrePDI, order_data){
			 $('#result').addClass('hidden').text("");
			 switchFinishUI(false);  
			 
					$('#generateOrderp_Pdi').unbind().click(function(){  
					
                        showHideMainLoader(true);
                        
						 if(isPrePDI){  
									console.log("Pre pdi generated"); 
									var newData = 	getNewDataFromPreview(order_data); 
								//	console.log(newData);
									var visualIssuesTable = $('#tblVisualPreview').prop('outerHTML');
									var HiddenIssuesTable = $('#tblHiddenPreview').prop('outerHTML');

//									console.log("old " +order_data);  
//									console.log(order_data.id);  
//									console.log(visualIssuesTable);  
									 
									//update src order id into description
									newData.description="src #"+order_data.id;
                               try{
                                 //console.log("signature"); 
                                   var signEnc= signaturePad2.toDataURL();
                                   if(signEnc != null ){
                                   newData.encSign = signEnc;
                                   }
                               }catch(err){
//                                 console.log("newData err "+err);
                               }
									
                             try{ 
//                                 console.log(newData.FullOrder);
                                 if(newData.FullOrder.customerInfo.shipToAddress){
                                    if(!(newData.FullOrder.customerInfo.shipToAddress.postalcode)){
                                        newData.FullOrder.customerInfo.shipToAddress.postalcode ="X1X1X1";
                                     }
                                 }
//                                 console.log(newData.FullOrder.customerInfo.shipToAddress.postalcode);
                                 
                             }catch(err){
//                                 console.log("newData err "+err);
                             }
                              try{
//                                 console.log("newData");
//                                 console.log(newData.FullOrder);
                                 if(newData.FullOrder.shipToAddress){
                                    if(!(newData.FullOrder.shipToAddress[0].postalcode)){
                                        newData.FullOrder.shipToAddress[0].postalcode ="X1X1X1";
                                     }
                                 } 
//                                 console.log(newData.FullOrder.shipToAddress[0].postalcode); 
                             }catch(err){
//                                 console.log("newData err "+err);
                             }
                             
                             
//                                 console.log("order_data.FullOrder");
//                                 console.log(order_data.FullOrder);
                              try{ 
                                  if(order_data.FullOrder.shipToAddress){  
                                             if( order_data.FullOrder.customerInfo.shipToAddress){  
                                                  order_data.FullOrder.shipToAddress = order_data.FullOrder.customerInfo.shipToAddress;
                                             } 
                                     }
                               }catch(err){
                                 console.log("newData err "+err);
                             }
                             
//                                 console.log("newData");
//                                 console.log(newData);
////                      for test only       
//                               try{
//                                       AddNotesToOrderGenerated(newData.FullOrder);
//                                }catch(err){
//                                        console.log("err  add to notes "+err); 
//                                }
////                      for test only  
                             
									//also add order# in the description
								kV.CreateOrderByTypePPDI(newData, visualIssuesTable, HiddenIssuesTable).done(function (result) {
										//	console.log("success "+result);
										
										   var createOrderId = JSON.parse(result).order.orderId; 
//										   console.log("success createOrderId "+createOrderId);
                                            try{
                                                AddNotesToOrderGenerated(JSON.parse(result).order);
                                            }catch(err){
                                                console.log("err add to notes "+err); 
                                            }
                                        
											var desc_ ="";
											if(order_data.FullOrder.description){
												desc_ = order_data.FullOrder.description; 
											}
                                   
										order_data.FullOrder.description = desc_+"Pre PDI order #"+createOrderId+"\n";
										try{
										  updateSelectedOrderStatus(order_data, _orderStatus[0], createOrderId );
											console.log("parent order updated");
                                        }catch(err){
											console.log("err updateSelectedOrderStatus "+ err);
                                            showHideAlertMessage("parent order updation failed",false, 200);  
                                         }
								           console.log("#"+createOrderId +" PRE PDI generated successfully");
                                           showHideAlertMessage("#"+createOrderId +" PRE PDI generated successfully",true, 400);  
//										   removeAllIssuesStored(order_data.id);
										   switchFinishUI(true);  
                                           showHideMainLoader(false); 
//                                    confirmationAlert("#"+createOrderId +" PRE PDI generated successfully");
                                    
									}).fail(function (result) {   
											console.log("err CreateOrderByTypePPDI "+result);
                                            showHideAlertMessage("pre pdi generation failed",false, 200);  
                                            showHideMainLoader(false);
									}); 
									
						 }else{
							 if(!isPrePDI){
									console.log("pdi generated"); 
									var newData = 	getNewDataFromPreview(order_data);
									var visualIssuesTable = $('#tblVisualPreview').prop('outerHTML');
									var HiddenIssuesTable = $('#tblHiddenPreview').prop('outerHTML');
					  
									//update src order id into description
									newData.description="src #"+order_data.id;
                                   try{
                                      //   console.log("signature"); 
                                         var signEnc= signaturePad2.toDataURL();
                                           if(signEnc != null ){
                                           newData.encSign =signEnc;
                                           }
                                       }catch(err){
        //                                 console.log("newData err "+err);
                                       }
                                 		
                             try{ 
//                                 console.log(newData.FullOrder);
                                 if(newData.FullOrder.customerInfo.shipToAddress){
                                    if(!(newData.FullOrder.customerInfo.shipToAddress.postalcode)){
                                        newData.FullOrder.customerInfo.shipToAddress.postalcode ="X1X1X1";
                                     }
                                 }
//                                 console.log(newData.FullOrder.customerInfo.shipToAddress.postalcode);
                                 
                             }catch(err){
//                                 console.log("newData err "+err);
                             }
                              try{
//                                 console.log("newData");
//                                 console.log(newData.FullOrder);
                                 if(newData.FullOrder.shipToAddress){
                                    if(!(newData.FullOrder.shipToAddress[0].postalcode)){
                                        newData.FullOrder.shipToAddress[0].postalcode ="X1X1X1";
                                     }
                                 } 
//                                 console.log(newData.FullOrder.shipToAddress[0].postalcode); 
                             }catch(err){
//                                 console.log("newData err "+err);
                             }
                                 
                             try{ 
                                  if(order_data.FullOrder.shipToAddress){  
                                             if( order_data.FullOrder.customerInfo.shipToAddress){  
                                                  order_data.FullOrder.shipToAddress = order_data.FullOrder.customerInfo.shipToAddress;
                                             } 
                                     }
                               }catch(err){
                                 console.log("newData err "+err);
                             }
                                 
									
									kV.CreateOrderByTypePDI(newData, visualIssuesTable, HiddenIssuesTable).done(function (result) {
											//console.log("success "+result);   
										   var createOrderId = JSON.parse(result).order.orderId; 
                                        
//										   console.log("success  createOrderId "+createOrderId);
                                            try{
                                                AddNotesToOrderGenerated(JSON.parse(result).order);
                                            }catch(err){
										          console.log("err  add to notes "+err); 
                                            }
										   
											var desc_ ="";
											if(order_data.FullOrder.description){
												desc_ = order_data.FullOrder.description; 
											}
										      order_data.FullOrder.description = desc_+"PDI order #"+createOrderId+"\n";
										
                                            try{
                                              updateSelectedOrderStatus(order_data, _orderStatus[1],createOrderId );
//                                                console.log("parent order updated");
                                            }catch(err){
                                                console.log("err updateSelectedOrderStatus "+result);
                                                showHideAlertMessage("parent order updation failed",false, 200);  
                                             } 
//											 removeAllIssuesStored(order_data.id);
                                        
//                                            confirmationAlert("#"+createOrderId +" PDI generated successfully");
								            console.log("#"+createOrderId +"  PDI generated successfully"); 
                                            showHideAlertMessage("#"+createOrderId +" PDI generated successfully",true, 200); 
											switchFinishUI(true);
                                           showHideMainLoader(false); 
									}).fail(function (result) {   
											console.log("err "+result);
                                            showHideAlertMessage("pdi generation failed",false, 200);  
                                           showHideMainLoader(false);
									});  
							 }
						 }
					}); 
				  
					   
		 }

//update pdi order
	 function InitUpdatePDI(isPrePDI, order_data){
			 $('#result').addClass('hidden').text("");
			 switchFinishUI(false);  
			 
					$('#updateOrderp_Pdi').unbind().click(function(){  
					
                        showHideMainLoader(true);
                        
						 if(isPrePDI){  
									console.log("Pre pdi updated"); 
									var newData = 	getNewDataFromPreview(order_data); 
								//	console.log(newData);
									var visualIssuesTable = $('#tblVisualPreview').prop('outerHTML');
									var HiddenIssuesTable = $('#tblHiddenPreview').prop('outerHTML');

//									console.log("old " +order_data);  
									console.log(order_data.id);  
									
									//update src order id into description
									newData.description="src #"+order_data.id;
                               try{
                                 //console.log("signature"); 
                                   var signEnc= signaturePad2.toDataURL();
                                   if(signEnc != null ){
                                   newData.encSign = signEnc;
                                   }
                               }catch(err){
//                                 console.log("newData err "+err);
                               }
									
                             try{ 
//                                 console.log(newData.FullOrder);
                                 if(newData.FullOrder.customerInfo.shipToAddress){
                                    if(!(newData.FullOrder.customerInfo.shipToAddress.postalcode)){
                                        newData.FullOrder.customerInfo.shipToAddress.postalcode ="X1X1X1";
                                     }
                                 }
//                                 console.log(newData.FullOrder.customerInfo.shipToAddress.postalcode);
                                 
                             }catch(err){
//                                 console.log("newData err "+err);
                             }
                              try{
//                                 console.log("newData");
//                                 console.log(newData.FullOrder);
                                 if(newData.FullOrder.shipToAddress){
                                    if(!(newData.FullOrder.shipToAddress[0].postalcode)){
                                        newData.FullOrder.shipToAddress[0].postalcode ="X1X1X1";
                                     }
                                 } 
//                                 console.log(newData.FullOrder.shipToAddress[0].postalcode); 
                             }catch(err){
//                                 console.log("newData err "+err);
                             }
                             
                             
//                                 console.log("order_data.FullOrder");
//                                 console.log(order_data.FullOrder);
                              try{ 
                                  if(order_data.FullOrder.shipToAddress){  
                                             if( order_data.FullOrder.customerInfo.shipToAddress){  
                                                  order_data.FullOrder.shipToAddress = [order_data.FullOrder.customerInfo.shipToAddress];
                                             } 
                                     }
                               }catch(err){
                                 console.log("newData err "+err);
                             }
                             
//                         
//                                 console.log("newData");
//                                 console.log(newData); 
//                             
//                                 console.log("OrderData"); 
//                             
//                             
                             var updateOrderDat = newData.FullOrder;
                             updateOrderDat.product[0].configuration.dateOfPossession = newData.dateOfPossession; 
                             updateOrderDat.product[0].configuration.vendorBuilderReference = newData.vendorRef;
                             updateOrderDat.product[0].configuration.lot = newData.lotNo;
                             updateOrderDat.product[0].configuration.plan = newData.PlanNo;
                             updateOrderDat.product[0].configuration.municipality = newData.Municipality;
                             updateOrderDat.product[0].configuration.condominiumProjectName = newData.condoProjectName;
                             updateOrderDat.product[0].configuration.level = newData.condoLevel;
                             updateOrderDat.product[0].configuration.unit = newData.condoUnit;
                             updateOrderDat.product[0].configuration.homeCivicAddress = newData.addressofInspectedHouse;
                             updateOrderDat.product[0].configuration.vendorBuilderName = newData.vendorName;
                                                            
                             updateOrderDat.product[0].configuration.pdiDate = newData.datecreated;
                             updateOrderDat.product[0].configuration.signature = newData.encSign;
                             updateOrderDat.product[0].configuration.visualIssues = visualIssuesTable; 
                             updateOrderDat.product[0].configuration.hiddenIssues = HiddenIssuesTable;
                             
//                             delete updateOrderDat.__metadata;
//                                 console.log(updateOrderDat); 
                             
//                             //                      for test only       
//                               try{
//                                       AddNotesToOrderGenerated(updateOrderDat);
//                                }catch(err){
//                                        console.log("err  add to notes "+err); 
//                                }
////                      for test only  
                             //for test delete/ archive current orderNotes 
//                             deleteIdsFromKV();
                             
                             kV.UpdateOrderByTypePPDI(updateOrderDat, visualIssuesTable, HiddenIssuesTable).done(function (result) {
//											console.log("success "+result);
										console.log("success  updated");
										console.log(JSON.parse(result));  
                                         
                                       AddNotesToOrderGenerated(JSON.parse(result).order);
                                  
                                 
                                            showHideAlertMessage("#"+JSON.parse(result).order.orderId +" PRE PDI updated successfully",true, 400);  
//										   removeAllIssuesStored(order_data.id);
										   switchFinishUI(true);    
                                           showHideMainLoader(false); 
//                                           confirmationAlert("#"+JSON.parse(result).order.orderId  +" PRE PDI updated successfully");
                                   
									}).fail(function (result) {   
											alert("error Updating PrePDI Order "+result);
											console.log("err UpdateOrderByTypePPDI "+result);
                                            showHideAlertMessage("pre pdi updated failed",false, 200);  
                                            showHideMainLoader(false);
									}); 
                       
						 }else{
							 if(!isPrePDI){
									
									console.log("Pre pdi updated"); 
									var newData = 	getNewDataFromPreview(order_data); 
								//	console.log(newData);
									var visualIssuesTable = $('#tblVisualPreview').prop('outerHTML');
									var HiddenIssuesTable = $('#tblHiddenPreview').prop('outerHTML');

//									console.log("old " +order_data);  
									console.log(order_data.id);  
									
									//update src order id into description
									newData.description="src #"+order_data.id;
                               try{
                                 //console.log("signature"); 
                                   var signEnc= signaturePad2.toDataURL();
                                   if(signEnc != null ){
                                   newData.encSign = signEnc;
                                   }
                               }catch(err){
//                                 console.log("newData err "+err);
                               }
									
                             try{ 
//                                 console.log(newData.FullOrder);
                                 if(newData.FullOrder.customerInfo.shipToAddress){
                                    if(!(newData.FullOrder.customerInfo.shipToAddress.postalcode)){
                                        newData.FullOrder.customerInfo.shipToAddress.postalcode ="X1X1X1";
                                     }
                                 }
//                                 console.log(newData.FullOrder.customerInfo.shipToAddress.postalcode);
                                 
                             }catch(err){
//                                 console.log("newData err "+err);
                             }
                              try{
//                                 console.log("newData");
//                                 console.log(newData.FullOrder);
                                 if(newData.FullOrder.shipToAddress){
                                    if(!(newData.FullOrder.shipToAddress[0].postalcode)){
                                        newData.FullOrder.shipToAddress[0].postalcode ="X1X1X1";
                                     }
                                 } 
//                                 console.log(newData.FullOrder.shipToAddress[0].postalcode); 
                             }catch(err){
//                                 console.log("newData err "+err);
                             }
                             
                             
//                                 console.log("order_data.FullOrder");
//                                 console.log(order_data.FullOrder);
                              try{ 
                                  if(order_data.FullOrder.shipToAddress){  
                                             if( order_data.FullOrder.customerInfo.shipToAddress){  
                                                  order_data.FullOrder.shipToAddress = [order_data.FullOrder.customerInfo.shipToAddress];
                                             } 
                                     }
                               }catch(err){
                                 console.log("newData err "+err);
                             }
                              
//                                 console.log("newData");
//                                 console.log(newData); 
                             
                                 console.log("OrderData"); 
                             
                             
                             var updateOrderDat = newData.FullOrder;
                             updateOrderDat.product[0].configuration.dateOfPossession = newData.dateOfPossession; 
                             updateOrderDat.product[0].configuration.vendorBuilderReference = newData.vendorRef;
                             updateOrderDat.product[0].configuration.lot = newData.lotNo;
                             updateOrderDat.product[0].configuration.plan = newData.PlanNo;
                             updateOrderDat.product[0].configuration.municipality = newData.Municipality;
                             updateOrderDat.product[0].configuration.condominiumProjectName = newData.condoProjectName;
                             updateOrderDat.product[0].configuration.level = newData.condoLevel;
                             updateOrderDat.product[0].configuration.unit = newData.condoUnit;
                             updateOrderDat.product[0].configuration.homeCivicAddress = newData.addressofInspectedHouse;
                             updateOrderDat.product[0].configuration.vendorBuilderName = newData.vendorName;
                                                            
                             updateOrderDat.product[0].configuration.pdiDate = newData.datecreated;
                             updateOrderDat.product[0].configuration.signature = newData.encSign;
                             updateOrderDat.product[0].configuration.visualIssues = visualIssuesTable; 
                             updateOrderDat.product[0].configuration.hiddenIssues = HiddenIssuesTable;
                             
//                             delete updateOrderDat.__metadata;
//                                 console.log(updateOrderDat); 
                             
                             kV.UpdateOrderByTypePDI(updateOrderDat, visualIssuesTable, HiddenIssuesTable).done(function (result) {
//											console.log("success "+result);
										console.log("success  updated");
										console.log(JSON.parse(result));  
                                     
                                            AddNotesToOrderGenerated(JSON.parse(result).order);
                                  
                                            showHideAlertMessage("#"+JSON.parse(result).order.orderId +"  PDI updated successfully",true, 400);  
//										   removeAllIssuesStored(order_data.id);
										   switchFinishUI(true);  
                                           showHideMainLoader(false); 
//                                    confirmationAlert("#"+JSON.parse(result).order.orderId  +" PDI updated successfully");
                                    
									}).fail(function (result) {   
											alert("error Updating PDI Order "+result);
											console.log("err UpdateOrderByTypePDI "+result);
                                            showHideAlertMessage("Pdi updated failed",false, 200);  
                                            showHideMainLoader(false);
									}); 
							 }
						 }
					}); 
				  
					   
		 }

//delete the ids stored in IssueDeleteIds in pdiIssuesHelper
function deleteIdsFromKV(){
//    IssueDeleteIds
//      console.log("Checking to Delete note"); 
//    debugger;
//      console.log(IssueDeleteIds); 
                if(IssueDeleteIds){
                 async.waterfall([ 
                     function(callback){   
                         
//                               console.log("notes found in topic ");
                                var noteIDs =null;
                                var NoteDetailsArray = []; 
                          
                                for(var i =0; i < IssueDeleteIds.length; i++){
                                     noteIDs = IssueDeleteIds[i];  
//                                    console.log("notes found in topic " +noteIDs);
                                    
                                    kV.getNoteDetailsByID(noteIDs).done(function (result){
                                        
                                        var modifiedOrd = JSON.parse(result);
                                        
//                                        console.log("modifiedOrd " + modifiedOrd);
                                        modifiedOrd.topic.archived = true;
                                        
//                                     console.log("NOTE psuhing " + i);
                                        NoteDetailsArray.push(modifiedOrd);
                                        
                                       if(!NoteDetailsArray[NoteDetailsArray.length]){ 
    //                                            console.log("reached end of array " + i);
    //                                            console.log("NoteDetailsArray " +NoteDetailsArray.length);
                                                callback(null, NoteDetailsArray);
                                        }
                              
                                    }).fail(function (result) {   
//                                                console.log("Couldnt Get the note details" +result);
                                                callback(null);
                                     });
                                     
                                 }  
                        },function(NoteDetails, callback){ 
                            
                            console.log("Note deleting now");
                            console.log(NoteDetails);
                            for(var i =0; i < NoteDetails.length; i++){
                                 
                            console.log(NoteDetails[i]);
                             kV.deleteNotes(NoteDetails[i]).done(function (result2){

                                 var onj = JSON.parse(result2);

                                 console.log("Deleted sucessfully");
                                console.log(onj);
      
                                 }).fail(function (result) {   
                                  console.log("Couldnt delete the note" +result);
                            });
                                
                            }
                          
                        }
                     
                     
                    ]);     
                }
}

function confirmationAlert(txt) { 
    if (confirm(txt +" \n click OK to go to HOME") == true) {
       switchUI(1);
    } else {
    }
}

		 
//<form id="uploadResourceForm" action="https://testing1.kloudville.com/upload?$service=system.import.createResourceUrl" method="post" enctype="multipart/form-data">
//  <input type="text" class="hidden" name="contextCategory" value="POST"/>
//  <input type="text" class="hidden" name="contextId" value="bcbcbe92-e3f7-4837-9e9f-098dd000f27"/> 
//  <input type="text" class="hidden" name="$service" value="system.import.createResourceUrl"/> 
//  <input type="file" class="hidden" name="hiddenFieldToTriggerFileUploadDetection" value=""/>
//  <input type="file" class="hidden" mime="image/png"/> 
//  <input type="file" name="file" /> 
// <button type="submit">Submit</button>
//</form>



//After generating the order add notes into it with that order Id
		 function AddNotesToOrderGenerated(order_data){
             if(NotesList.length >0){  
             for(var i =0; i < NotesList.length; i++){
                  
                 imgs_arr_forsingleIssue =[];  //reseting img array
                 var noteTitle = NotesList[i].title;
                 var noteDesc = NotesList[i].description;
                 
                 
                 var uniqueID = NotesList[i].uniqueID;  
                 var NoteChanged = NotesList[i].trEditted;
                 console.log(NotesList);
                  
                 //create notes only if issues contains the images
                 if( NotesList[i].imgsArray ){
                     //upload only if editted note = true
//                     if(NoteChanged){
                     var imgContainerArr = NotesList[i];  
                     console.log("noteTitle "+ noteTitle +" noteDesc "+
                                 noteDesc +" imgContainerArr "+ imgContainerArr +" "+ uniqueID);
                   
                     
                     console.log("checking if note exists"); 
                     //check if the note topic id exists
                     var trNoteExists = checkifTopicExists(order_data.orderId, uniqueID);
                     
                     //if true
                     //then add post to that topic
                     if(trNoteExists){
                            console.log("checking if note exists " + trNoteExists); 
                            //first check if content is not same i.e. unchanged 
                                     console.log("upload trNoteExists " +order_data.orderId+" "+ noteTitle+" "+ noteDesc );

                          createPostInNoteKV(imgContainerArr, order_data, noteTitle, noteDesc, uniqueID);
                     }
                     //else 
                     //create note topic
                     else{ 
                            console.log("checking if note exists " + trNoteExists); 
                                async.waterfall([
                                function(callback) {  
        //                             console.log("Note created with " +noteDesc);
        //                            console.log(NotesList[i]);  
        //                             console.log("Note created with " +noteDesc);

//                                     console.log("upload attachment " +order_data.orderId+" "+ noteTitle+" "+ noteDesc );
                                     uploadAttachmentTOKV(imgContainerArr, order_data.orderId, noteTitle, noteDesc, uniqueID, callback);            //Send the unique ID 

                                },
                                function(arg1, arg2, arg3, arg4, unqId, callback) { 
                                    //attachment, order_data.orderId, noteTitle, noteDesc
        //                            console.log("received in 2nd size ---"); 
        //                            console.log(arg1);  
        //                            console.log("arg2"); 
        //                            console.log(arg2);  
                                        console.log("received create note " +arg2+" "+ arg3+" "+ arg4 );
                                         //if there is any images with the notes then create a topic in kV
                                           kV.createNote(unqId, arg2, arg3, arg4, arg1).done(function (result) {
                                                var ResultTopicObj  = JSON.parse(result);
        //                                        postObj = ResultTopicObj.createTopicRequest.post;
//                                                console.log(ResultTopicObj);
        //                                        console.log("note added to new order");  

                                               //after success adding remove the notes
        //											 removeAllIssuesStored(arg2);
                                                callback(null, "");
                                         }).fail(function (result) {   
                                                 console.log("err createNote "+result); 
                                         });   
                                }
                            ], function(err, arr){//------->Flow complete
                                      if(err){
                                        console.log(err);  } 
                            }); 
                         
                     }
//                 }
                     //delete notes whicch has been deleted from the list
                     deleteIdsFromKV();
                      
                 }  
                        
             } 
            }
     }


//check if the note exists in the kv
        function checkifTopicExists(orderId, uniqueID){ 
            var result = false;
            console.log("checkifTopicExists");
            console.log(_wholeorder_tr_data);
        try{
                                if(_wholeorder_tr_data.KVnotes){  
                              var notes_Arr = _wholeorder_tr_data.KVnotes.multipageSearchResult.result;
//                                      console.log("_wholeorder_tr_data" + _wholeorder_tr_data);
//                                            console.log(_wholeorder_tr_data); 
                                        for(var i = 0; i< (notes_Arr).length; i++){
                                            var noteId = (notes_Arr[i]).id;  
//                                                    console.log("uniqueID "+ uniqueID +" noteId "+ noteId);
                                            if(uniqueID == noteId){ 
                                                    console.log("noteId exists for id " + noteId); 
                                                result = true;
                                            }

                                    }  
                                }      
        }catch(err){
                        console.log("checkifTopicExists exists err " + err); 
            
        }
            console.log("checkifTopicExists " +result);
            return result;
        }
  

function createPostInNoteKV(imgContainerArr, order_data, noteTitle, noteDesc, topicId){
         async.waterfall([
                        function(callback) {  
//                             console.log("Post created with " +noteDesc);
//                            console.log(imgContainerArr);  
//                             console.log("Post created with " +noteDesc);  
//                            console.log("TopicId retreived success "+ topicId);
//                 console.log("upload attachment " +order_data.orderId+" "+ noteTitle+" "+ noteDesc );
                            uploadAttachmentTOKV(imgContainerArr, order_data.orderId, noteTitle, noteDesc, topicId,  callback); 
//                              callback(null, null, order_data.orderId, noteTitle,  noteDesc);
                        },
                        function(arg1, arg2, arg3, arg4, unqId, callback) { 
                            //attachment, order_data.orderId, noteTitle, noteDesc
//                            console.log("received in 2nd size ---"); 
//                            console.log(arg1);  
//                            console.log("arg2"); 
//                            console.log(arg2);  
//                 console.log("received create Post " +arg2+" "+ arg3+" "+ arg4 );
                            
                                    console.log("Creating post Note adding to post  "+ arg2 +" " + arg3);
                           
                                 //if there is any images with the notes then create a topic in kV
                                   kV.createPostInNote(topicId, arg3+" "+ arg4, arg1) .done(function (result) {
                                        var ResultTopicObj  = JSON.parse(result);
//                                        postObj = ResultTopicObj.createTopicRequest.post;
//                                        console.log(postObj);
//                                        console.log("note added to new order");  
                                   
                                       //after success adding remove the notes
//											 removeAllIssuesStored(arg2);
                                        callback(null, "");
                                 }).fail(function (result) {   
                                         console.log("err createNote "+result); 
                                 });   
                        }
                    ], function(err, arr){//------->Flow complete
                              if(err){
                                console.log(err);  } 
                            }); 
    
}
  
//upload image first and then add that to attachment of note
function uploadAttachmentTOKV(imgContainerArr, order_orderId, noteTitle, noteDesc, uniqueID, callback){
     try{ 
         //makes sure the array is emptied before every note
           imgs_arr_forsingleIssue =  []; 
          var local_issuesContainer = [];
          
         var inc = 0;
         var imgContain = imgContainerArr.imgsArray;  
//         debugger;
         
         //recursive function
         UploadIt();
//         debugger;
         
         function UploadIt()
         { 
             if (!imgContain[inc]) {
//                 console.log("finished issue " +noteDesc+ " size of final "+local_issuesContainer.length );
             //   console.log(local_issuesContainer);   
//                 console.log("finished upload " +order_orderId+" "+ noteTitle+" "+ noteDesc );
                 
                 //call to update order base here 
                callback(null, local_issuesContainer, order_orderId, noteTitle,  noteDesc, uniqueID);
                    return;
             }
//             debugger;
             var ImgDat = imgContain[inc];
//             alert(ImgDat.length);
//             debugger;
               
             try{
                 if (!(ImgDat.indexOf("data")>=0)){
//                        console.log("its a url");
                         var Attachment_ ={};
                         var resourceUrl = {};
 
                         var splittedString = ImgDat.split('.com/');
                         var splittedSlashString = ImgDat.split('/');
                         var imagePath = splittedString[splittedString.length -1];
//                         imagePath = imagePath.replace(/\//g, '\\/');

                         resourceUrl.__metadata= { "type":"com.kloudville.system.import.resourceUrl"};
                         resourceUrl.name=  splittedSlashString[splittedSlashString.length -1];
                         resourceUrl.url= imagePath;
                         resourceUrl.mime= "image\/png";
                     
                        Attachment_.resourceUrl = resourceUrl;
 
                        local_issuesContainer.push(resourceUrl);  
                      inc++;
                      UploadIt();
                     
                 }else{
//                 console.log("its a base 64");
                        
             var GenFileName = "issueImg_uniqueID-"+ uniqueID +"_" +(new Date()).getTime()+'.png';

                var formdata = new FormData(); 
                formdata.append('contextCategory', "POST");
                formdata.append('contextId', "bcbcbe92-e3f7-4837-9e9f-098dd000f27");
                formdata.append('$service', "system.import.createResourceUrl");
                formdata.append('hiddenFieldToTriggerFileUploadDetection', ""); 
//                formdata.append('mime', "image/png");  
                formdata.append('mime', "mime");    

              var _file = null;
             try{
               
                 _file =dataURItoBlobNew(ImgDat, GenFileName);
                formdata.append('file', _file,GenFileName); 
              
             }catch(errr){
                 console.log("Blob Error "+ errr);
             }
                console.log("file data ");
//                console.log(ImgDat);
             
              try{
                            basil = new window.Basil(UsSpaceoptions); 
                            var isloggedIN =  basil.get('IsloggedUser'); 
//                  debugger;
                  
                         if(isloggedIN){ 
                                var user = basil.get("lgU");
                                var pass =  basil.get("lgP");
                                var url =   basil.get("lgR");
                                url += url.endsWith("/") ? "" : "/";
                                var loginDat = {
                                            userLogin: {
                                                __metadata: {
                                                    type: 'com.kloudville.idm.userLogin'
                                                },
                                                user: user,
                                                password: pass
                                            }
                                        };
 
                            var xhr = createXHR();
                             
                             
                                 xhr.withCredentials = true;
                                 xhr.addEventListener("readystatechange", function () {
                                  if (this.readyState === 4) {

                                        var JsonObj =JSON.parse(this.responseText);
                                        local_issuesContainer.push(JsonObj.resourceUrl); 
//                                        imgs_arr_forsingleIssue.push(JsonObj.resourceUrl); 
        //                              var attachment = JsonObj.resourceUrl;
                                        console.log(JsonObj); 
//                                      alert ("image upload done");
//                                        console.log(local_issuesContainer);  
                                      inc++;
                                      UploadIt();
                                  }
                                });

                             
//                             console.log("url "+url);
                             
                            xhr.timeout = 2000; // time in milliseconds
                            xhr.open("POST", url+ "service/idm/authentication/login");
                            xhr.setRequestHeader("Content-Type", "application/json");
                            xhr.send( JSON.stringify(loginDat));
 
                            xhr.open("POST", url+ "upload?$service=system.import.createResourceUrl",true);
                            xhr.send(formdata);  
                        }    
                }catch(err){
                  console.log(" error uploading "+err);
                  alert ("image upload error "+err); 
                } 
                     
                 }
             }catch(err1){
                 console.log("not url error "+err1 );
             }
             
         }        
      }
      catch(e){
          console.log("sending error  "+e);
      } 
}
  
        function createXHR() {
    try {
        return new XMLHttpRequest();
    } catch (e) {
        try {
            return new ActiveXObject("Microsoft.XMLHTTP");
        } catch (e) {
            return new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
}


//converting base 64 data into file
        function dataURLtoFile(dataurl, filename) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
        bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while(n--){
        u8arr[n] = bstr.charCodeAt(n);
    }
    return new File([u8arr], filename, {type:mime});
}
	

        function dataURItoBlobNew(dataURI, filename) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia],  {type:mimeString});
}

		 function getNewDataFromPreview(oldData){
			  var newData =  new  OrderData2 (oldData.id,
													oldData.orderdisplayName,
																		oldData.houseType,
																		(new Date($('#signedDate').datepicker("getDate"))).toISOString(),
																		oldData.customerName,
																		($('#homeAddressline1').html()).replace("&nbsp", " "),
																		($('#UnitEnrol1').html()).replace("&nbsp", " "),
																		(new Date($('#dateOfPossession').datepicker("getDate"))).toISOString(),
																		($('#vendorBuilderReference').html()).replace("&nbsp", " "),
																		($('#lotNum').html()).replace("&nbsp", " "),
																		($('#planNum').html()).replace("&nbsp", " "),
																		($('#MuniciplaityName').html()).replace("&nbsp", " "),
																		($('#CondoProjectName').html()).replace("&nbsp", " "),
																		($('#levelNum').html()).replace("&nbsp", " "),
																		($('#UnitNum').html()).replace("&nbsp", " "),
																		($('#vendorBuilderName').html()).replace("&nbsp", " "),
																		oldData.representativeName, 
																		oldData.purchaserName2,
																		oldData.DesignateName,
																		oldData.FullOrder
																		);  
			 return newData;
		 }
		  
		 function removeAllIssuesStored(orderid){  
                    basil_issues = new window.Basil(issues_options); 
             
                    basil_issues.remove(loggerUser+"_"+orderid+'_Visual'+'_'+prePDIselected);  
					basil_issues.remove(loggerUser+"_"+orderid+'_Hidden'+'_'+prePDIselected);  
               
		 }
		  
	//update after updating the prdi order	 
		function updateSelectedOrderStatus(oldWholeOrder, status_, createOrderId){
				 
					var newWholeOrder = oldWholeOrder.FullOrder;
					newWholeOrder.orderSource = status_;
					 
            console.log("updateSelectedOrderStatus");
            console.log(oldWholeOrder);
            
            if(oldWholeOrder.FullOrder.shipToAddress){ 
                if(!newWholeOrder.shipToAddress){
                    
                    var oldShipToAddress =  oldWholeOrder.FullOrder.shipToAddress;
                    newWholeOrder.shipToAddress = [oldShipToAddress];
                }else{
                    var oldShipAdd = newWholeOrder.shipToAddress;
                    if(!(oldShipAdd  instanceof Array)){
                        newWholeOrder.shipToAddress = [oldShipAdd];
                    }
                } 
            }
            var oldCharge = newWholeOrder.cartTotal;
            if(!oldCharge[0].cost){oldCharge[0].cost = null;}
            if(!oldCharge[0].costRevenue){oldCharge[0].costRevenue = null;}
            if(!oldCharge[0].costQuantity){oldCharge[0].costQuantity = null;}
            if(!oldCharge[0].charge){oldCharge[0].charge = [];}
            if(!oldCharge[0].message){oldCharge[0].message = [];}
            
             newWholeOrder.cartTotal = oldCharge;
            
					console.log(newWholeOrder); 
						kV.UpdateOrderStatusforPDI(newWholeOrder).done(function (result) {
								console.log("success update parent "+JSON.parse(result));
								console.log(JSON.parse(result));
						//	$('#result').removeClass('hidden').text(" pre pdi generated successfully"); 
								           
                                    confirmationAlert("#"+createOrderId +" created successfully");
						}).fail(function (result) {   
								console.log("err "+result);
                                     showHideAlertMessage("unable to update src order "+ result,false, 200);
                           
								           
					//			$('#result').removeClass('hidden').text(" pre pdi generate failed");
					});   
			}			
			 
	//after finish generating pdi then hide the buttons of generation		
		function switchFinishUI(GenerateFinish){
				if(GenerateFinish){ 
                $(".pdi_Navbar").addClass("hidden");
                    if(!$('#generateOrderp_Pdi').hasClass( "hidden" )){
                            $('#generateOrderp_Pdi').addClass('hidden');  
                    }  
                    if(!$('#updateOrderp_Pdi').hasClass( "hidden" )){
                            $('#updateOrderp_Pdi').addClass('hidden');  
                    } 
//                    switchUI(1); 
                } 
			}
		 

//signature function 
        function loadSignatureUI(){  
        canvas2 =null;
        signaturePad2 = null;
    
        canvas2=document.getElementById("signatureCanvas");
     
        signaturePad2 = new SignaturePad(canvas2);
        signaturePad2.minWidth = 1;
        signaturePad2.maxWidth = 2;
        signaturePad2.penColor = "rgb(4, 4, 4)";
    
 
        $(clearbtnSign).unbind().click(function () { 
                     signaturePad2.clear();     
//                    $('#signatureheading').removeClass('hidden'); 
                canvas2.src = defaultColor; 
           
         });
        $(canvas2).awesomeCursor('pencil', {
          color: '#d4a190',
            size: 24,
            hotspot: 'left bottom',
            outline: 'black'
        });
     
}

        function HideAndSaveSign(){
        //    console.log("You clicked hide and save!");  

            var dataURL = signaturePad.toDataURL(); 

            if(dataURL != null){ 
                $('#signature-pad').addClass('hidden');
                $('#pdiPreview').removeClass('hidden');
                $('#signatureheading').addClass('hidden');

                 $("#canvasImg").attr("src", dataURL);
        //         document.getElementById('canvasImg').src = dataURL;  
            } 
}
 
// Adjust canvas coordinate space taking into account pixel ratio,
// Make it look crisp on mobile devices.
// This also causes canvas to be cleared.
        function resizeCanvas() {
            // When zoomed out to less than 100%, for some very strange reason,
            // some browsers report devicePixelRatio as less than 1
            // and only part of the canvas is cleared then.
            var ratio =  Math.max(window.devicePixelRatio || 1, 1);
            canvas.width = canvas.offsetWidth * ratio;
            canvas.height = canvas.offsetHeight * ratio;
            canvas.getContext("2d").scale(ratio, ratio); 
        }
